export const tracksArgentina = {
  "tracks": [
    {
      "album": {
        "album_type": "album",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
            },
            "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
            "id": "5srsgtoePlsp0eQIXmta1O",
            "name": "Sandro",
            "type": "artist",
            "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/12h6RCRmJ7mgOzl9h7uigs"
        },
        "href": "https://api.spotify.com/v1/albums/12h6RCRmJ7mgOzl9h7uigs",
        "id": "12h6RCRmJ7mgOzl9h7uigs",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b2739e8536a134e5a6304e676cdf",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e029e8536a134e5a6304e676cdf",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d000048519e8536a134e5a6304e676cdf",
            "width": 64
          }
        ],
        "name": "Una Muchacha Y Una Guitarra",
        "release_date": "1968-04-13",
        "release_date_precision": "day",
        "total_tracks": 15,
        "type": "album",
        "uri": "spotify:album:12h6RCRmJ7mgOzl9h7uigs"
      },
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
          },
          "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
          "id": "5srsgtoePlsp0eQIXmta1O",
          "name": "Sandro",
          "type": "artist",
          "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
        }
      ],
      "disc_number": 1,
      "duration_ms": 213333,
      "explicit": false,
      "external_ids": {
        "isrc": "ARF109700590"
      },
      "external_urls": {
        "spotify": "https://open.spotify.com/track/2KuP2WnaXV7hiHn9KLq27E"
      },
      "href": "https://api.spotify.com/v1/tracks/2KuP2WnaXV7hiHn9KLq27E",
      "id": "2KuP2WnaXV7hiHn9KLq27E",
      "is_local": false,
      "is_playable": true,
      "name": "Porque Yo Te Amo",
      "popularity": 43,
      "preview_url": "https://p.scdn.co/mp3-preview/159c1bd5ba64ccfcc7d2af031433b9d776506620?cid=821bd86f751d403ea10afa3294e70c93",
      "track_number": 8,
      "type": "track",
      "uri": "spotify:track:2KuP2WnaXV7hiHn9KLq27E"
    },
    {
      "album": {
        "album_type": "album",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
            },
            "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
            "id": "5srsgtoePlsp0eQIXmta1O",
            "name": "Sandro",
            "type": "artist",
            "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/5KWGSBbAvNO1ycytBsPiT9"
        },
        "href": "https://api.spotify.com/v1/albums/5KWGSBbAvNO1ycytBsPiT9",
        "id": "5KWGSBbAvNO1ycytBsPiT9",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b273aa73b5f891c1a270c22b0e8c",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e02aa73b5f891c1a270c22b0e8c",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d00004851aa73b5f891c1a270c22b0e8c",
            "width": 64
          }
        ],
        "name": "Sandro De América",
        "release_date": "1969-04-13",
        "release_date_precision": "day",
        "total_tracks": 14,
        "type": "album",
        "uri": "spotify:album:5KWGSBbAvNO1ycytBsPiT9"
      },
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
          },
          "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
          "id": "5srsgtoePlsp0eQIXmta1O",
          "name": "Sandro",
          "type": "artist",
          "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
        }
      ],
      "disc_number": 1,
      "duration_ms": 153026,
      "explicit": false,
      "external_ids": {
        "isrc": "ARF109700599"
      },
      "external_urls": {
        "spotify": "https://open.spotify.com/track/2xWNLtRPgcQwjTWjhncnJF"
      },
      "href": "https://api.spotify.com/v1/tracks/2xWNLtRPgcQwjTWjhncnJF",
      "id": "2xWNLtRPgcQwjTWjhncnJF",
      "is_local": false,
      "is_playable": true,
      "name": "Rosa... Rosa",
      "popularity": 44,
      "preview_url": "https://p.scdn.co/mp3-preview/c7d10fa05270240880fbc030aca8ede4acd0f578?cid=821bd86f751d403ea10afa3294e70c93",
      "track_number": 3,
      "type": "track",
      "uri": "spotify:track:2xWNLtRPgcQwjTWjhncnJF"
    },
    {
      "album": {
        "album_type": "album",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
            },
            "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
            "id": "5srsgtoePlsp0eQIXmta1O",
            "name": "Sandro",
            "type": "artist",
            "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/5AoO2XU5pild42akpbfv9s"
        },
        "href": "https://api.spotify.com/v1/albums/5AoO2XU5pild42akpbfv9s",
        "id": "5AoO2XU5pild42akpbfv9s",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b273d1bb9a809cd811edde14f19e",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e02d1bb9a809cd811edde14f19e",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d00004851d1bb9a809cd811edde14f19e",
            "width": 64
          }
        ],
        "name": "La Magia De Sandro",
        "release_date": "1968",
        "release_date_precision": "year",
        "total_tracks": 14,
        "type": "album",
        "uri": "spotify:album:5AoO2XU5pild42akpbfv9s"
      },
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
          },
          "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
          "id": "5srsgtoePlsp0eQIXmta1O",
          "name": "Sandro",
          "type": "artist",
          "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
        }
      ],
      "disc_number": 1,
      "duration_ms": 196560,
      "explicit": false,
      "external_ids": {
        "isrc": "ARF109600440"
      },
      "external_urls": {
        "spotify": "https://open.spotify.com/track/3xef9cUovCAdblrNkP1CRF"
      },
      "href": "https://api.spotify.com/v1/tracks/3xef9cUovCAdblrNkP1CRF",
      "id": "3xef9cUovCAdblrNkP1CRF",
      "is_local": false,
      "is_playable": true,
      "name": "Penas",
      "popularity": 43,
      "preview_url": "https://p.scdn.co/mp3-preview/2d4a6751ad15c4ca616b461bc99397a3b1b9f519?cid=821bd86f751d403ea10afa3294e70c93",
      "track_number": 9,
      "type": "track",
      "uri": "spotify:track:3xef9cUovCAdblrNkP1CRF"
    },
    {
      "album": {
        "album_type": "album",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
            },
            "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
            "id": "5srsgtoePlsp0eQIXmta1O",
            "name": "Sandro",
            "type": "artist",
            "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/3ZTUibaHdDqDhkOiVNDOWC"
        },
        "href": "https://api.spotify.com/v1/albums/3ZTUibaHdDqDhkOiVNDOWC",
        "id": "3ZTUibaHdDqDhkOiVNDOWC",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b273531f954f9c3c16f79a98a118",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e02531f954f9c3c16f79a98a118",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d00004851531f954f9c3c16f79a98a118",
            "width": 64
          }
        ],
        "name": "Sandro Dúos",
        "release_date": "2018-02-23",
        "release_date_precision": "day",
        "total_tracks": 12,
        "type": "album",
        "uri": "spotify:album:3ZTUibaHdDqDhkOiVNDOWC"
      },
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
          },
          "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
          "id": "5srsgtoePlsp0eQIXmta1O",
          "name": "Sandro",
          "type": "artist",
          "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
        },
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/0dE9ooTNz8iEKIKItRI66t"
          },
          "href": "https://api.spotify.com/v1/artists/0dE9ooTNz8iEKIKItRI66t",
          "id": "0dE9ooTNz8iEKIKItRI66t",
          "name": "Il Volo",
          "type": "artist",
          "uri": "spotify:artist:0dE9ooTNz8iEKIKItRI66t"
        }
      ],
      "disc_number": 1,
      "duration_ms": 250202,
      "explicit": false,
      "external_ids": {
        "isrc": "ARF101800030"
      },
      "external_urls": {
        "spotify": "https://open.spotify.com/track/0SnuzHo8GsSiHrZIJinAMI"
      },
      "href": "https://api.spotify.com/v1/tracks/0SnuzHo8GsSiHrZIJinAMI",
      "id": "0SnuzHo8GsSiHrZIJinAMI",
      "is_local": false,
      "is_playable": true,
      "name": "Penumbras",
      "popularity": 41,
      "preview_url": "https://p.scdn.co/mp3-preview/c999faf05893ac6daea13dc52a4175a1096f0854?cid=821bd86f751d403ea10afa3294e70c93",
      "track_number": 4,
      "type": "track",
      "uri": "spotify:track:0SnuzHo8GsSiHrZIJinAMI"
    },
    {
      "album": {
        "album_type": "album",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
            },
            "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
            "id": "5srsgtoePlsp0eQIXmta1O",
            "name": "Sandro",
            "type": "artist",
            "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/1pCY0eZXXvrDUqQVVybNLA"
        },
        "href": "https://api.spotify.com/v1/albums/1pCY0eZXXvrDUqQVVybNLA",
        "id": "1pCY0eZXXvrDUqQVVybNLA",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b273b7cf4844ab7e50506ffa1a27",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e02b7cf4844ab7e50506ffa1a27",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d00004851b7cf4844ab7e50506ffa1a27",
            "width": 64
          }
        ],
        "name": "Muchacho",
        "release_date": "1970-10-27",
        "release_date_precision": "day",
        "total_tracks": 11,
        "type": "album",
        "uri": "spotify:album:1pCY0eZXXvrDUqQVVybNLA"
      },
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
          },
          "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
          "id": "5srsgtoePlsp0eQIXmta1O",
          "name": "Sandro",
          "type": "artist",
          "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
        }
      ],
      "disc_number": 1,
      "duration_ms": 177906,
      "explicit": false,
      "external_ids": {
        "isrc": "ARF109900506"
      },
      "external_urls": {
        "spotify": "https://open.spotify.com/track/0vjbYWVR4N08uWureijhBX"
      },
      "href": "https://api.spotify.com/v1/tracks/0vjbYWVR4N08uWureijhBX",
      "id": "0vjbYWVR4N08uWureijhBX",
      "is_local": false,
      "is_playable": true,
      "name": "La Vida Sigue Igual",
      "popularity": 40,
      "preview_url": "https://p.scdn.co/mp3-preview/3ec98d7d275b330a5886196d959eca148d8a0113?cid=821bd86f751d403ea10afa3294e70c93",
      "track_number": 3,
      "type": "track",
      "uri": "spotify:track:0vjbYWVR4N08uWureijhBX"
    },
    {
      "album": {
        "album_type": "album",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
            },
            "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
            "id": "5srsgtoePlsp0eQIXmta1O",
            "name": "Sandro",
            "type": "artist",
            "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/5AoO2XU5pild42akpbfv9s"
        },
        "href": "https://api.spotify.com/v1/albums/5AoO2XU5pild42akpbfv9s",
        "id": "5AoO2XU5pild42akpbfv9s",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b273d1bb9a809cd811edde14f19e",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e02d1bb9a809cd811edde14f19e",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d00004851d1bb9a809cd811edde14f19e",
            "width": 64
          }
        ],
        "name": "La Magia De Sandro",
        "release_date": "1968",
        "release_date_precision": "year",
        "total_tracks": 14,
        "type": "album",
        "uri": "spotify:album:5AoO2XU5pild42akpbfv9s"
      },
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
          },
          "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
          "id": "5srsgtoePlsp0eQIXmta1O",
          "name": "Sandro",
          "type": "artist",
          "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
        }
      ],
      "disc_number": 1,
      "duration_ms": 122160,
      "explicit": false,
      "external_ids": {
        "isrc": "ARF109700595"
      },
      "external_urls": {
        "spotify": "https://open.spotify.com/track/4ADkfiY6Dejdr3ukiLF4MJ"
      },
      "href": "https://api.spotify.com/v1/tracks/4ADkfiY6Dejdr3ukiLF4MJ",
      "id": "4ADkfiY6Dejdr3ukiLF4MJ",
      "is_local": false,
      "is_playable": true,
      "name": "Tengo",
      "popularity": 39,
      "preview_url": "https://p.scdn.co/mp3-preview/0aa2ee5522ea978be25aa48d4ad7002dcd7b2aaf?cid=821bd86f751d403ea10afa3294e70c93",
      "track_number": 1,
      "type": "track",
      "uri": "spotify:track:4ADkfiY6Dejdr3ukiLF4MJ"
    },
    {
      "album": {
        "album_type": "album",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
            },
            "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
            "id": "5srsgtoePlsp0eQIXmta1O",
            "name": "Sandro",
            "type": "artist",
            "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/192TUA3B8bftPsFc2IdbHO"
        },
        "href": "https://api.spotify.com/v1/albums/192TUA3B8bftPsFc2IdbHO",
        "id": "192TUA3B8bftPsFc2IdbHO",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b27388a003ee9b96dfbe3cbbf1d6",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e0288a003ee9b96dfbe3cbbf1d6",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d0000485188a003ee9b96dfbe3cbbf1d6",
            "width": 64
          }
        ],
        "name": "Grandes Exitos",
        "release_date": "2018-10-31",
        "release_date_precision": "day",
        "total_tracks": 20,
        "type": "album",
        "uri": "spotify:album:192TUA3B8bftPsFc2IdbHO"
      },
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
          },
          "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
          "id": "5srsgtoePlsp0eQIXmta1O",
          "name": "Sandro",
          "type": "artist",
          "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
        }
      ],
      "disc_number": 1,
      "duration_ms": 229395,
      "explicit": false,
      "external_ids": {
        "isrc": "ARG991027107"
      },
      "external_urls": {
        "spotify": "https://open.spotify.com/track/1RjoLiMqDLG92chuQas1Gu"
      },
      "href": "https://api.spotify.com/v1/tracks/1RjoLiMqDLG92chuQas1Gu",
      "id": "1RjoLiMqDLG92chuQas1Gu",
      "is_local": false,
      "is_playable": true,
      "name": "Mi Amigo el Puma - En Vivo",
      "popularity": 39,
      "preview_url": "https://p.scdn.co/mp3-preview/b75fb1d1493fe4cdf330434869cc30749868ac54?cid=821bd86f751d403ea10afa3294e70c93",
      "track_number": 1,
      "type": "track",
      "uri": "spotify:track:1RjoLiMqDLG92chuQas1Gu"
    },
    {
      "album": {
        "album_type": "album",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
            },
            "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
            "id": "5srsgtoePlsp0eQIXmta1O",
            "name": "Sandro",
            "type": "artist",
            "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/5AoO2XU5pild42akpbfv9s"
        },
        "href": "https://api.spotify.com/v1/albums/5AoO2XU5pild42akpbfv9s",
        "id": "5AoO2XU5pild42akpbfv9s",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b273d1bb9a809cd811edde14f19e",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e02d1bb9a809cd811edde14f19e",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d00004851d1bb9a809cd811edde14f19e",
            "width": 64
          }
        ],
        "name": "La Magia De Sandro",
        "release_date": "1968",
        "release_date_precision": "year",
        "total_tracks": 14,
        "type": "album",
        "uri": "spotify:album:5AoO2XU5pild42akpbfv9s"
      },
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
          },
          "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
          "id": "5srsgtoePlsp0eQIXmta1O",
          "name": "Sandro",
          "type": "artist",
          "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
        }
      ],
      "disc_number": 1,
      "duration_ms": 194666,
      "explicit": false,
      "external_ids": {
        "isrc": "ARF109700594"
      },
      "external_urls": {
        "spotify": "https://open.spotify.com/track/5DezmVf1OjcIOZgoPtozVq"
      },
      "href": "https://api.spotify.com/v1/tracks/5DezmVf1OjcIOZgoPtozVq",
      "id": "5DezmVf1OjcIOZgoPtozVq",
      "is_local": false,
      "is_playable": true,
      "name": "Así",
      "popularity": 30,
      "preview_url": "https://p.scdn.co/mp3-preview/484aeb5d1eaaeaef896235949f2a06a4ff5a1250?cid=821bd86f751d403ea10afa3294e70c93",
      "track_number": 4,
      "type": "track",
      "uri": "spotify:track:5DezmVf1OjcIOZgoPtozVq"
    },
    {
      "album": {
        "album_type": "album",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
            },
            "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
            "id": "5srsgtoePlsp0eQIXmta1O",
            "name": "Sandro",
            "type": "artist",
            "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/5KWGSBbAvNO1ycytBsPiT9"
        },
        "href": "https://api.spotify.com/v1/albums/5KWGSBbAvNO1ycytBsPiT9",
        "id": "5KWGSBbAvNO1ycytBsPiT9",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b273aa73b5f891c1a270c22b0e8c",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e02aa73b5f891c1a270c22b0e8c",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d00004851aa73b5f891c1a270c22b0e8c",
            "width": 64
          }
        ],
        "name": "Sandro De América",
        "release_date": "1969-04-13",
        "release_date_precision": "day",
        "total_tracks": 14,
        "type": "album",
        "uri": "spotify:album:5KWGSBbAvNO1ycytBsPiT9"
      },
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
          },
          "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
          "id": "5srsgtoePlsp0eQIXmta1O",
          "name": "Sandro",
          "type": "artist",
          "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
        }
      ],
      "disc_number": 1,
      "duration_ms": 188960,
      "explicit": false,
      "external_ids": {
        "isrc": "ARF109700600"
      },
      "external_urls": {
        "spotify": "https://open.spotify.com/track/3VN3vYr5Ez7kjx6th8U9U1"
      },
      "href": "https://api.spotify.com/v1/tracks/3VN3vYr5Ez7kjx6th8U9U1",
      "id": "3VN3vYr5Ez7kjx6th8U9U1",
      "is_local": false,
      "is_playable": true,
      "name": "El Maniquí",
      "popularity": 33,
      "preview_url": "https://p.scdn.co/mp3-preview/5ea6e04475e068c7e4f8f907c0b7319653280079?cid=821bd86f751d403ea10afa3294e70c93",
      "track_number": 9,
      "type": "track",
      "uri": "spotify:track:3VN3vYr5Ez7kjx6th8U9U1"
    },
    {
      "album": {
        "album_type": "album",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
            },
            "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
            "id": "5srsgtoePlsp0eQIXmta1O",
            "name": "Sandro",
            "type": "artist",
            "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/3ZTUibaHdDqDhkOiVNDOWC"
        },
        "href": "https://api.spotify.com/v1/albums/3ZTUibaHdDqDhkOiVNDOWC",
        "id": "3ZTUibaHdDqDhkOiVNDOWC",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b273531f954f9c3c16f79a98a118",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e02531f954f9c3c16f79a98a118",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d00004851531f954f9c3c16f79a98a118",
            "width": 64
          }
        ],
        "name": "Sandro Dúos",
        "release_date": "2018-02-23",
        "release_date_precision": "day",
        "total_tracks": 12,
        "type": "album",
        "uri": "spotify:album:3ZTUibaHdDqDhkOiVNDOWC"
      },
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/5srsgtoePlsp0eQIXmta1O"
          },
          "href": "https://api.spotify.com/v1/artists/5srsgtoePlsp0eQIXmta1O",
          "id": "5srsgtoePlsp0eQIXmta1O",
          "name": "Sandro",
          "type": "artist",
          "uri": "spotify:artist:5srsgtoePlsp0eQIXmta1O"
        },
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/2AZOALDIBORfbzKTuliwdJ"
          },
          "href": "https://api.spotify.com/v1/artists/2AZOALDIBORfbzKTuliwdJ",
          "id": "2AZOALDIBORfbzKTuliwdJ",
          "name": "Cristian Castro",
          "type": "artist",
          "uri": "spotify:artist:2AZOALDIBORfbzKTuliwdJ"
        }
      ],
      "disc_number": 1,
      "duration_ms": 162541,
      "explicit": false,
      "external_ids": {
        "isrc": "ARF101800027"
      },
      "external_urls": {
        "spotify": "https://open.spotify.com/track/4sbOKwH3FjteQgddj1AusF"
      },
      "href": "https://api.spotify.com/v1/tracks/4sbOKwH3FjteQgddj1AusF",
      "id": "4sbOKwH3FjteQgddj1AusF",
      "is_local": false,
      "is_playable": true,
      "name": "Rosa Rosa",
      "popularity": 34,
      "preview_url": "https://p.scdn.co/mp3-preview/d41d04866ec04c45c4052d7785a3362727a43ae6?cid=821bd86f751d403ea10afa3294e70c93",
      "track_number": 1,
      "type": "track",
      "uri": "spotify:track:4sbOKwH3FjteQgddj1AusF"
    }
  ]
};