export const playlist = {
    "collaborative": false,
    "description": "A playlist for testing pourposes",
    "external_urls": {
      "spotify": "https://open.spotify.com/playlist/3cEYpjA9oz9GiPac4AsH4n"
    },
    "followers": {
      "href": null,
      "total": 82
    },
    "href": "https://api.spotify.com/v1/playlists/3cEYpjA9oz9GiPac4AsH4n",
    "id": "3cEYpjA9oz9GiPac4AsH4n",
    "images": [
      {
        "height": null,
        "url": "https://pl.scdn.co/images/pl/default/15e1e401aca06139b92bb116834a8324d03d4fd1",
        "width": null
      }
    ],
    "name": "Spotify Web API Testing playlist",
    "owner": {
      "display_name": "JMPerez²",
      "external_urls": {
        "spotify": "https://open.spotify.com/user/jmperezperez"
      },
      "href": "https://api.spotify.com/v1/users/jmperezperez",
      "id": "jmperezperez",
      "type": "user",
      "uri": "spotify:user:jmperezperez"
    },
    "primary_color": null,
    "public": true,
    "snapshot_id": "MTcsZDhlNTBiODE0ZTExZDExYjM4NGFlZmFlOTQ1NGE1NTk3ZjNmM2RmOQ==",
    "tracks": {
      "href": "https://api.spotify.com/v1/playlists/3cEYpjA9oz9GiPac4AsH4n/tracks?offset=0&limit=100&market=ES",
      "items": [
        {
          "added_at": "2015-01-15T12:39:22Z",
          "added_by": {
            "external_urls": {
              "spotify": "https://open.spotify.com/user/jmperezperez"
            },
            "href": "https://api.spotify.com/v1/users/jmperezperez",
            "id": "jmperezperez",
            "type": "user",
            "uri": "spotify:user:jmperezperez"
          },
          "is_local": false,
          "primary_color": null,
          "track": {
            "album": {
              "album_type": "album",
              "artists": [
                {
                  "external_urls": {
                    "spotify": "https://open.spotify.com/artist/5I8r2w4hf7OYp2cunjihxJ"
                  },
                  "href": "https://api.spotify.com/v1/artists/5I8r2w4hf7OYp2cunjihxJ",
                  "id": "5I8r2w4hf7OYp2cunjihxJ",
                  "name": "Kularis",
                  "type": "artist",
                  "uri": "spotify:artist:5I8r2w4hf7OYp2cunjihxJ"
                }
              ],
              "external_urls": {
                "spotify": "https://open.spotify.com/album/2pANdqPvxInB0YvcDiw4ko"
              },
              "href": "https://api.spotify.com/v1/albums/2pANdqPvxInB0YvcDiw4ko",
              "id": "2pANdqPvxInB0YvcDiw4ko",
              "images": [
                {
                  "height": 640,
                  "url": "https://i.scdn.co/image/ab67616d0000b273ce6d0eef0c1ce77e5f95bbbc",
                  "width": 640
                },
                {
                  "height": 300,
                  "url": "https://i.scdn.co/image/ab67616d00001e02ce6d0eef0c1ce77e5f95bbbc",
                  "width": 300
                },
                {
                  "height": 64,
                  "url": "https://i.scdn.co/image/ab67616d00004851ce6d0eef0c1ce77e5f95bbbc",
                  "width": 64
                }
              ],
              "name": "Progressive Psy Trance Picks Vol.8",
              "release_date": "2012-04-02",
              "release_date_precision": "day",
              "total_tracks": 20,
              "type": "album",
              "uri": "spotify:album:2pANdqPvxInB0YvcDiw4ko"
            },
            "artists": [
              {
                "external_urls": {
                  "spotify": "https://open.spotify.com/artist/6eSdhw46riw2OUHgMwR8B5"
                },
                "href": "https://api.spotify.com/v1/artists/6eSdhw46riw2OUHgMwR8B5",
                "id": "6eSdhw46riw2OUHgMwR8B5",
                "name": "Odiseo",
                "type": "artist",
                "uri": "spotify:artist:6eSdhw46riw2OUHgMwR8B5"
              }
            ],
            "disc_number": 1,
            "duration_ms": 376000,
            "episode": false,
            "explicit": false,
            "external_ids": {
              "isrc": "DEKC41200989"
            },
            "external_urls": {
              "spotify": "https://open.spotify.com/track/4rzfv0JLZfVhOhbSQ8o5jZ"
            },
            "href": "https://api.spotify.com/v1/tracks/4rzfv0JLZfVhOhbSQ8o5jZ",
            "id": "4rzfv0JLZfVhOhbSQ8o5jZ",
            "is_local": false,
            "is_playable": true,
            "name": "Api",
            "popularity": 2,
            "preview_url": "https://p.scdn.co/mp3-preview/c440fa9ff320fb74629f8477bff45b1a377897ab?cid=821bd86f751d403ea10afa3294e70c93",
            "track": true,
            "track_number": 10,
            "type": "track",
            "uri": "spotify:track:4rzfv0JLZfVhOhbSQ8o5jZ"
          },
          "video_thumbnail": {
            "url": null
          }
        },
        {
          "added_at": "2015-01-15T12:40:03Z",
          "added_by": {
            "external_urls": {
              "spotify": "https://open.spotify.com/user/jmperezperez"
            },
            "href": "https://api.spotify.com/v1/users/jmperezperez",
            "id": "jmperezperez",
            "type": "user",
            "uri": "spotify:user:jmperezperez"
          },
          "is_local": false,
          "primary_color": null,
          "track": {
            "album": {
              "album_type": "album",
              "artists": [
                {
                  "external_urls": {
                    "spotify": "https://open.spotify.com/artist/5VQE4WOzPu9h3HnGLuBoA6"
                  },
                  "href": "https://api.spotify.com/v1/artists/5VQE4WOzPu9h3HnGLuBoA6",
                  "id": "5VQE4WOzPu9h3HnGLuBoA6",
                  "name": "Vlasta Marek",
                  "type": "artist",
                  "uri": "spotify:artist:5VQE4WOzPu9h3HnGLuBoA6"
                }
              ],
              "external_urls": {
                "spotify": "https://open.spotify.com/album/5Opp8ursJ6XoTTcqElvAJG"
              },
              "href": "https://api.spotify.com/v1/albums/5Opp8ursJ6XoTTcqElvAJG",
              "id": "5Opp8ursJ6XoTTcqElvAJG",
              "images": [
                {
                  "height": 640,
                  "url": "https://i.scdn.co/image/ab67616d0000b273cf921f1b12eb4ecb99e1b4d4",
                  "width": 640
                },
                {
                  "height": 300,
                  "url": "https://i.scdn.co/image/ab67616d00001e02cf921f1b12eb4ecb99e1b4d4",
                  "width": 300
                },
                {
                  "height": 64,
                  "url": "https://i.scdn.co/image/ab67616d00004851cf921f1b12eb4ecb99e1b4d4",
                  "width": 64
                }
              ],
              "name": "Tibetan Bowls",
              "release_date": "2000",
              "release_date_precision": "year",
              "total_tracks": 6,
              "type": "album",
              "uri": "spotify:album:5Opp8ursJ6XoTTcqElvAJG"
            },
            "artists": [
              {
                "external_urls": {
                  "spotify": "https://open.spotify.com/artist/5VQE4WOzPu9h3HnGLuBoA6"
                },
                "href": "https://api.spotify.com/v1/artists/5VQE4WOzPu9h3HnGLuBoA6",
                "id": "5VQE4WOzPu9h3HnGLuBoA6",
                "name": "Vlasta Marek",
                "type": "artist",
                "uri": "spotify:artist:5VQE4WOzPu9h3HnGLuBoA6"
              }
            ],
            "disc_number": 1,
            "duration_ms": 730066,
            "episode": false,
            "explicit": false,
            "external_ids": {
              "isrc": "NLA930601339"
            },
            "external_urls": {
              "spotify": "https://open.spotify.com/track/64IL0ZrOV7F6BDPm5fhfUa"
            },
            "href": "https://api.spotify.com/v1/tracks/64IL0ZrOV7F6BDPm5fhfUa",
            "id": "64IL0ZrOV7F6BDPm5fhfUa",
            "is_local": false,
            "is_playable": true,
            "linked_from": {
              "external_urls": {
                "spotify": "https://open.spotify.com/track/5o3jMYOSbaVz3tkgwhELSV"
              },
              "href": "https://api.spotify.com/v1/tracks/5o3jMYOSbaVz3tkgwhELSV",
              "id": "5o3jMYOSbaVz3tkgwhELSV",
              "type": "track",
              "uri": "spotify:track:5o3jMYOSbaVz3tkgwhELSV"
            },
            "name": "Is",
            "popularity": 2,
            "preview_url": "https://p.scdn.co/mp3-preview/7fada9f304c72420a7cbb2c531e8ef71db81ed53?cid=821bd86f751d403ea10afa3294e70c93",
            "track": true,
            "track_number": 2,
            "type": "track",
            "uri": "spotify:track:64IL0ZrOV7F6BDPm5fhfUa"
          },
          "video_thumbnail": {
            "url": null
          }
        },
        {
          "added_at": "2015-01-15T12:22:30Z",
          "added_by": {
            "external_urls": {
              "spotify": "https://open.spotify.com/user/jmperezperez"
            },
            "href": "https://api.spotify.com/v1/users/jmperezperez",
            "id": "jmperezperez",
            "type": "user",
            "uri": "spotify:user:jmperezperez"
          },
          "is_local": false,
          "primary_color": null,
          "track": {
            "album": {
              "album_type": "album",
              "artists": [
                {
                  "external_urls": {
                    "spotify": "https://open.spotify.com/artist/066X20Nz7iquqkkCW6Jxy6"
                  },
                  "href": "https://api.spotify.com/v1/artists/066X20Nz7iquqkkCW6Jxy6",
                  "id": "066X20Nz7iquqkkCW6Jxy6",
                  "name": "LCD Soundsystem",
                  "type": "artist",
                  "uri": "spotify:artist:066X20Nz7iquqkkCW6Jxy6"
                }
              ],
              "external_urls": {
                "spotify": "https://open.spotify.com/album/4hnqM0JK4CM1phwfq1Ldyz"
              },
              "href": "https://api.spotify.com/v1/albums/4hnqM0JK4CM1phwfq1Ldyz",
              "id": "4hnqM0JK4CM1phwfq1Ldyz",
              "images": [
                {
                  "height": 640,
                  "url": "https://i.scdn.co/image/ab67616d0000b273ee0d0dce888c6c8a70db6e8b",
                  "width": 640
                },
                {
                  "height": 300,
                  "url": "https://i.scdn.co/image/ab67616d00001e02ee0d0dce888c6c8a70db6e8b",
                  "width": 300
                },
                {
                  "height": 64,
                  "url": "https://i.scdn.co/image/ab67616d00004851ee0d0dce888c6c8a70db6e8b",
                  "width": 64
                }
              ],
              "name": "This Is Happening",
              "release_date": "2010-05-17",
              "release_date_precision": "day",
              "total_tracks": 9,
              "type": "album",
              "uri": "spotify:album:4hnqM0JK4CM1phwfq1Ldyz"
            },
            "artists": [
              {
                "external_urls": {
                  "spotify": "https://open.spotify.com/artist/066X20Nz7iquqkkCW6Jxy6"
                },
                "href": "https://api.spotify.com/v1/artists/066X20Nz7iquqkkCW6Jxy6",
                "id": "066X20Nz7iquqkkCW6Jxy6",
                "name": "LCD Soundsystem",
                "type": "artist",
                "uri": "spotify:artist:066X20Nz7iquqkkCW6Jxy6"
              }
            ],
            "disc_number": 1,
            "duration_ms": 401440,
            "episode": false,
            "explicit": false,
            "external_ids": {
              "isrc": "US4GE1000022"
            },
            "external_urls": {
              "spotify": "https://open.spotify.com/track/4Cy0NHJ8Gh0xMdwyM9RkQm"
            },
            "href": "https://api.spotify.com/v1/tracks/4Cy0NHJ8Gh0xMdwyM9RkQm",
            "id": "4Cy0NHJ8Gh0xMdwyM9RkQm",
            "is_local": false,
            "is_playable": true,
            "name": "All I Want",
            "popularity": 49,
            "preview_url": "https://p.scdn.co/mp3-preview/1dd960a5c02c5330a0d6578672971314d85adb75?cid=821bd86f751d403ea10afa3294e70c93",
            "track": true,
            "track_number": 4,
            "type": "track",
            "uri": "spotify:track:4Cy0NHJ8Gh0xMdwyM9RkQm"
          },
          "video_thumbnail": {
            "url": null
          }
        },
        {
          "added_at": "2015-01-15T12:40:35Z",
          "added_by": {
            "external_urls": {
              "spotify": "https://open.spotify.com/user/jmperezperez"
            },
            "href": "https://api.spotify.com/v1/users/jmperezperez",
            "id": "jmperezperez",
            "type": "user",
            "uri": "spotify:user:jmperezperez"
          },
          "is_local": false,
          "primary_color": null,
          "track": {
            "album": {
              "album_type": "album",
              "artists": [
                {
                  "external_urls": {
                    "spotify": "https://open.spotify.com/artist/272ArH9SUAlslQqsSgPJA2"
                  },
                  "href": "https://api.spotify.com/v1/artists/272ArH9SUAlslQqsSgPJA2",
                  "id": "272ArH9SUAlslQqsSgPJA2",
                  "name": "Glenn Horiuchi Trio",
                  "type": "artist",
                  "uri": "spotify:artist:272ArH9SUAlslQqsSgPJA2"
                }
              ],
              "external_urls": {
                "spotify": "https://open.spotify.com/album/2usKFntxa98WHMcyW6xJBz"
              },
              "href": "https://api.spotify.com/v1/albums/2usKFntxa98WHMcyW6xJBz",
              "id": "2usKFntxa98WHMcyW6xJBz",
              "images": [
                {
                  "height": 640,
                  "url": "https://i.scdn.co/image/ab67616d0000b2738b7447ac3daa1da18811cf7b",
                  "width": 640
                },
                {
                  "height": 300,
                  "url": "https://i.scdn.co/image/ab67616d00001e028b7447ac3daa1da18811cf7b",
                  "width": 300
                },
                {
                  "height": 64,
                  "url": "https://i.scdn.co/image/ab67616d000048518b7447ac3daa1da18811cf7b",
                  "width": 64
                }
              ],
              "name": "Glenn Horiuchi Trio / Gelenn Horiuchi Quartet: Mercy / Jump Start / Endpoints / Curl Out / Earthworks / Mind Probe / Null Set / Another Space (A)",
              "release_date": "2011-04-01",
              "release_date_precision": "day",
              "total_tracks": 8,
              "type": "album",
              "uri": "spotify:album:2usKFntxa98WHMcyW6xJBz"
            },
            "artists": [
              {
                "external_urls": {
                  "spotify": "https://open.spotify.com/artist/272ArH9SUAlslQqsSgPJA2"
                },
                "href": "https://api.spotify.com/v1/artists/272ArH9SUAlslQqsSgPJA2",
                "id": "272ArH9SUAlslQqsSgPJA2",
                "name": "Glenn Horiuchi Trio",
                "type": "artist",
                "uri": "spotify:artist:272ArH9SUAlslQqsSgPJA2"
              }
            ],
            "disc_number": 1,
            "duration_ms": 358760,
            "episode": false,
            "explicit": false,
            "external_ids": {
              "isrc": "USB8U1025969"
            },
            "external_urls": {
              "spotify": "https://open.spotify.com/track/6hvFrZNocdt2FcKGCSY5NI"
            },
            "href": "https://api.spotify.com/v1/tracks/6hvFrZNocdt2FcKGCSY5NI",
            "id": "6hvFrZNocdt2FcKGCSY5NI",
            "is_local": false,
            "is_playable": true,
            "name": "Endpoints",
            "popularity": 0,
            "preview_url": "https://p.scdn.co/mp3-preview/14301ef7f4f489d4a80a05afc34532b522cdeff0?cid=821bd86f751d403ea10afa3294e70c93",
            "track": true,
            "track_number": 2,
            "type": "track",
            "uri": "spotify:track:6hvFrZNocdt2FcKGCSY5NI"
          },
          "video_thumbnail": {
            "url": null
          }
        },
        {
          "added_at": "2015-01-15T12:41:10Z",
          "added_by": {
            "external_urls": {
              "spotify": "https://open.spotify.com/user/jmperezperez"
            },
            "href": "https://api.spotify.com/v1/users/jmperezperez",
            "id": "jmperezperez",
            "type": "user",
            "uri": "spotify:user:jmperezperez"
          },
          "is_local": false,
          "primary_color": null,
          "track": {
            "album": {
              "album_type": "album",
              "artists": [
                {
                  "external_urls": {
                    "spotify": "https://open.spotify.com/artist/2KftmGt9sk1yLjsAoloC3M"
                  },
                  "href": "https://api.spotify.com/v1/artists/2KftmGt9sk1yLjsAoloC3M",
                  "id": "2KftmGt9sk1yLjsAoloC3M",
                  "name": "Zucchero",
                  "type": "artist",
                  "uri": "spotify:artist:2KftmGt9sk1yLjsAoloC3M"
                }
              ],
              "external_urls": {
                "spotify": "https://open.spotify.com/album/0RxtWmjDExug0K5PZhhttP"
              },
              "href": "https://api.spotify.com/v1/albums/0RxtWmjDExug0K5PZhhttP",
              "id": "0RxtWmjDExug0K5PZhhttP",
              "images": [
                {
                  "height": 640,
                  "url": "https://i.scdn.co/image/1fb9bcb9fe709824907c483c8c53c8ed9e2a662e",
                  "width": 640
                },
                {
                  "height": 300,
                  "url": "https://i.scdn.co/image/a98aab0518b9a4480de8d2b7a74494866e08615d",
                  "width": 300
                },
                {
                  "height": 64,
                  "url": "https://i.scdn.co/image/9f8d291d95e6c7050fa56fa93c83de69aa3d7fbf",
                  "width": 64
                }
              ],
              "name": "All The Best",
              "release_date": "2007-01-01",
              "release_date_precision": "day",
              "total_tracks": 18,
              "type": "album",
              "uri": "spotify:album:0RxtWmjDExug0K5PZhhttP"
            },
            "artists": [
              {
                "external_urls": {
                  "spotify": "https://open.spotify.com/artist/2KftmGt9sk1yLjsAoloC3M"
                },
                "href": "https://api.spotify.com/v1/artists/2KftmGt9sk1yLjsAoloC3M",
                "id": "2KftmGt9sk1yLjsAoloC3M",
                "name": "Zucchero",
                "type": "artist",
                "uri": "spotify:artist:2KftmGt9sk1yLjsAoloC3M"
              }
            ],
            "disc_number": 1,
            "duration_ms": 176066,
            "episode": false,
            "explicit": false,
            "external_ids": {
              "isrc": "ITUM70701043"
            },
            "external_urls": {
              "spotify": "https://open.spotify.com/track/6dWqHAuGLmsjvUxOe9S2t6"
            },
            "href": "https://api.spotify.com/v1/tracks/6dWqHAuGLmsjvUxOe9S2t6",
            "id": "6dWqHAuGLmsjvUxOe9S2t6",
            "is_local": false,
            "is_playable": true,
            "linked_from": {
              "external_urls": {
                "spotify": "https://open.spotify.com/track/2E2znCPaS8anQe21GLxcvJ"
              },
              "href": "https://api.spotify.com/v1/tracks/2E2znCPaS8anQe21GLxcvJ",
              "id": "2E2znCPaS8anQe21GLxcvJ",
              "type": "track",
              "uri": "spotify:track:2E2znCPaS8anQe21GLxcvJ"
            },
            "name": "You Are So Beautiful",
            "popularity": 20,
            "preview_url": null,
            "track": true,
            "track_number": 18,
            "type": "track",
            "uri": "spotify:track:6dWqHAuGLmsjvUxOe9S2t6"
          },
          "video_thumbnail": {
            "url": null
          }
        }
      ],
      "limit": 100,
      "next": null,
      "offset": 0,
      "previous": null,
      "total": 5
    },
    "type": "playlist",
    "uri": "spotify:playlist:3cEYpjA9oz9GiPac4AsH4n"
  };
  