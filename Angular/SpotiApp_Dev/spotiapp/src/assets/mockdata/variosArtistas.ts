export const variosArtistas: any[] =  [
     {
       "external_urls": {
         "spotify": "https://open.spotify.com/artist/2CIMQHirSU0MQqyYHq0eOx"
       },
       "followers": {
         "href": null,
         "total": 2379065
       },
       "genres": [
         "big room",
         "canadian electronic",
         "edm",
         "electro house",
         "progressive house"
       ],
       "href": "https://api.spotify.com/v1/artists/2CIMQHirSU0MQqyYHq0eOx",
       "id": "2CIMQHirSU0MQqyYHq0eOx",
       "images": [
         {
           "height": 640,
           "url": "https://i.scdn.co/image/9effaa1fae5b51804692a309651083437c8f29fd",
           "width": 640
         },
         {
           "height": 320,
           "url": "https://i.scdn.co/image/0f81eb9ffa88afa5689ddf2ea41865d64b07149d",
           "width": 320
         },
         {
           "height": 160,
           "url": "https://i.scdn.co/image/46d9f505d6db8c6a3282c3f1573157d7b3e9f604",
           "width": 160
         }
       ],
       "name": "deadmau5",
       "popularity": 72,
       "type": "artist",
       "uri": "spotify:artist:2CIMQHirSU0MQqyYHq0eOx"
     },
     {
       "external_urls": {
         "spotify": "https://open.spotify.com/artist/57dN52uHvrHOxijzpIgu3E"
       },
       "followers": {
         "href": null,
         "total": 486591
       },
       "genres": [
         "alternative dance",
         "new rave"
       ],
       "href": "https://api.spotify.com/v1/artists/57dN52uHvrHOxijzpIgu3E",
       "id": "57dN52uHvrHOxijzpIgu3E",
       "images": [
         {
           "height": 693,
           "url": "https://i.scdn.co/image/2f0c6c465a83cd196e651e3d4e7625ba799a6f60",
           "width": 1000
         },
         {
           "height": 444,
           "url": "https://i.scdn.co/image/4e3e13c8b993bde9898e49509fb9ae121636e05f",
           "width": 640
         },
         {
           "height": 139,
           "url": "https://i.scdn.co/image/dc68dd24b45b74ecce9d4ed486423673d683ced3",
           "width": 200
         },
         {
           "height": 44,
           "url": "https://i.scdn.co/image/4e55ca05d4f336a2fa0e3062a7ec9778a201e8bc",
           "width": 63
         }
       ],
       "name": "Ratatat",
       "popularity": 70,
       "type": "artist",
       "uri": "spotify:artist:57dN52uHvrHOxijzpIgu3E"
     },
     {
       "external_urls": {
         "spotify": "https://open.spotify.com/artist/1vCWHaC5f2uS3yhpwWbIA6"
       },
       "followers": {
         "href": null,
         "total": 16693508
       },
       "genres": [
         "big room",
         "edm",
         "pop"
       ],
       "href": "https://api.spotify.com/v1/artists/1vCWHaC5f2uS3yhpwWbIA6",
       "id": "1vCWHaC5f2uS3yhpwWbIA6",
       "images": [
         {
           "height": 640,
           "url": "https://i.scdn.co/image/81b19a403109c4fe528ee3972137127b85be9519",
           "width": 640
         },
         {
           "height": 320,
           "url": "https://i.scdn.co/image/9c0d8fa969a9f5db6ff860203d6880a125e501d2",
           "width": 320
         },
         {
           "height": 160,
           "url": "https://i.scdn.co/image/c55bc6f57b6bb297425c3ae694f92672dcf0e2c2",
           "width": 160
         }
       ],
       "name": "Avicii",
       "popularity": 89,
       "type": "artist",
       "uri": "spotify:artist:1vCWHaC5f2uS3yhpwWbIA6"
     }
   ]
 
   // const objPrueba = {
   //    key: 'algo',
   //    arr: [1,2,3]
   // }

   // objPrueba.arr.forEach(num => console)