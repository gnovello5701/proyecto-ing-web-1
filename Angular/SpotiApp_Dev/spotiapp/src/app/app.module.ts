import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// Importar rutas
import { ROUTES } from './app.routes';

// Pipes
import { NoimagePipe } from './pipes/noimage.pipe';
import { DomseguroPipe } from './pipes/domseguro.pipe';

//Componentes
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { TuBibliotecaComponent  } from './components/tu-biblioteca/tu-biblioteca.component';
import { TarjetaListaComponent} from './components/tarjetas/tarjeta-lista/tarjeta-lista.component';
import { TopHeaderComponent } from './components/top-header/top-header.component';
import { FilaComponent } from './components/fila/fila.component';
import { GeneroCardComponent } from './components/tarjetas/genero-card/genero-card.component';
import { FooterComponent } from './components/footer/footer.component';
import { PlaylistComponent } from './components/shared/playlist/playlist.component';
import { TrackPlaylistComponent } from './components/shared/tracks/trackPlaylist/trackPlaylist.component';
import { ArtistaComponent } from './components/shared/artista/artista.component';
import { TusPlaylistsComponent } from './components/tu-biblioteca/tusCosas/tus-playlists/tus-playlists.component';
import { TusAlbumsComponent } from './components/tu-biblioteca/tusCosas/tus-albums/tus-albums.component';
import { TusArtistasComponent } from './components/tu-biblioteca/tusCosas/tus-artistas/tus-artistas.component';
import { AudioService } from './services/audio-ss.service';
import { CallbackComponent } from './components/callback/callback.component';
import { LoginComponent } from './components/login/login.component';
import { GeneroComponent } from './components/shared/genero/genero.component';
import { AlbumComponent } from './components/shared/album/album.component';
import { TrackAlbumComponent } from './components/shared/tracks/track-album/track-album.component';
import { TrackArtistaComponent } from './components/shared/tracks/track-artista/track-artista.component';
import { RegisterComponent } from './components/register/register.component';
import { PlayerComponent } from './components/player/player.component';
import { CrearPlaylistComponent } from './components/crear-playlist/crear-playlist.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    NavbarComponent,
    NoimagePipe,
    DomseguroPipe,
    TarjetaListaComponent,
    TopHeaderComponent,
    TuBibliotecaComponent,
    FilaComponent,
    GeneroCardComponent,
    FooterComponent,
    PlaylistComponent,
    TrackPlaylistComponent,
    ArtistaComponent,
    TusPlaylistsComponent,
    TusAlbumsComponent,
    TusArtistasComponent,
    CallbackComponent,
    LoginComponent,
    GeneroComponent,
    AlbumComponent,
    TrackAlbumComponent,
    TrackArtistaComponent,
    RegisterComponent,
    PlayerComponent,
    CrearPlaylistComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot( ROUTES, { useHash: true } )
  ],
  providers: [AudioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
