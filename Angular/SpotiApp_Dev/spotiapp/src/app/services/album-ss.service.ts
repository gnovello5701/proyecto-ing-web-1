import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { TrackModel } from '../models/track.model';
import { AlbumModel } from '../models/album.model';

const url = environment.localhost;

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  loading: boolean;
  error: boolean;

  constructor(private http: HttpClient) { }

  async getAlbumsTracks(id: string) {
    console.log("En get Tracks de un album")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('id', id);
    try {
        let response = await this.http.get(url + '/album/tracks', { headers: headers, params: params })
            .toPromise() as TrackModel[];
            console.log("data de getAlbumsTracks-->",response);
            return response;
    } catch (error) {
        console.log("Error  en getAlbumsTracks-->",error);
    }
  }
  
  async getAlbum(id: string) {
    console.log("En get un album")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('id', id);
    try {
        let response = await this.http.get(url + '/album', { headers: headers, params: params })
            .toPromise() as AlbumModel;
            console.log("data de getAlbum-->",response)
            return response;
    } catch (error) {
        
        console.log("Error en getAlbum-->",error);
    }
  }
  
  async getVariosAlbums(id: string) {
    console.log("En get varios albums")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('id', id);
    try {
        let response = await this.http.get(url + '/album/varios', { headers: headers, params: params })
            .toPromise() as AlbumModel[];
            console.log("data de getVariosAlbums-->",response)
            return response;
    } catch (error) {
        console.log("Error en getVariosAlbums-->",error);
    }
  }
  
}
