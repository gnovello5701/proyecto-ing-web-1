import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { TrackModel } from '../models/track.model';

const url = environment.localhost;

@Injectable({
  providedIn: 'root'
})
export class TrackService {

  constructor(private http: HttpClient) { }

  async getAnalisisAudioTrack(id: string) {
    console.log("En get Analisis del audio del track")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('id', id);
    try {
        let response = await this.http.get(url + '/track/analisisAudio', { headers: headers, params: params })
            .toPromise();
            console.log("data de getAnalisisAudioTrack-->",response)
            return response;
    } catch (error) {
        console.log("Error en getAnalisisAudioTrack-->",error);
    }
  }


  async getCaracteristicasDeUnTrack(id: string) {
    console.log("En get caracteristicas del track")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('id', id);
    try {
        let response = await this.http.get(url + '/track/caracteristicas/uno', { headers: headers, params: params })
            .toPromise();
            console.log("data de getCaracteristicasDeUnTrack-->",response)
            return response;
    } catch (error) {
        console.log("Error en getCaracteristicasDeUnTrack-->",error);
    }
  }

  async getCaracteristicasVariosTracks(id: string) {
    console.log("En get caracteristicas de varios tracks")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('id', id);
    try {
        let response = await this.http.get(url + '/track/caracteristicas/varios', { headers: headers, params: params })
            .toPromise();
            console.log("data de getCaracteristicasVariosTracks-->",response)
            return response;
    } catch (error) {
        console.log("Error en getCaracteristicasVariosTracks-->",error);
    }
  }

  async getVariosTracks(id: string) {
    console.log("En get varios tracks")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('id', id);
    try {
        let response = await this.http.get(url + '/tracks', { headers: headers, params: params })
            .toPromise() as TrackModel[];
            console.log("data de getVariosTracks-->",response)
            return response;
    } catch (error) {
        console.log("Error en getVariosTracks-->",error);
    }
  }

  async getTrack(id: string) {
    console.log("En get track")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('id', id);
    try {
        let response = await this.http.get(url + '/track', { headers: headers, params: params })
            .toPromise() as TrackModel;
            console.log("data de getTrack-->",response)
            return response;
    } catch (error) {
        console.log("Error en getTrack-->",error);
    }
  }



}
