import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const url = environment.localhost;
const my_client_id = '4e08fae57975438294428f1a07d3873c';
const redirect_uri = environment.localhostFront + '/#/home';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

    async logins() {
      console.log("En loginSpotify")
      let headers = new HttpHeaders({
          'Content-Type': 'application/json'
      });
      try {
          let response = await this.http.get(url + '/login', { headers: headers})
              .toPromise();
              var obj = JSON.parse(response.toString());
              console.log("data de login-->",obj)
              return obj;
      } catch (error) {
          console.log("Error en login-->",error);
      }
    }


}
