import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ArtistModel } from '../models/artist.model';
import { TrackModel } from '../models/track.model';
import { AlbumModel } from '../models/album.model';

const url = environment.localhost;

@Injectable({
  providedIn: 'root'
})
export class ArtistService {
  
  loading: boolean;
  error: boolean;

  constructor(private http: HttpClient) {
     this.loading = true;
     this.error = false;
 }

 async getArtista(id: string) {
  console.log("En get artista")
  let headers = new HttpHeaders({
      'Content-Type': 'application/json'
  });
  let params = new HttpParams().set('id', id);
  try {
      let response = await this.http.get(url + '/artista', { headers: headers, params: params })
          .toPromise() as ArtistModel;
          console.log("data de getArtista --> ",response)
          return response;
    } catch (error) {
      console.log("Error de getArtista --> ",error);
    }
  }
  async getArtistaVarios(id: string) {
     console.log("En get varios artistas")
     let headers = new HttpHeaders({
         'Content-Type': 'application/json'
     });
     let params = new HttpParams().set('id', id);
     try {
         let response = await this.http.get(url + '/artista/varios', { headers: headers, params: params })
             .toPromise() as ArtistModel[];
             console.log("data de getArtistaVarios --> ",response)
             return response;
     } catch (error) {
         console.log("Error de getArtistaVarios --> ",error);
     }
  }
  
  async getArtistaTracks(id: string) {
     console.log("En get Tracks de artista")
     let headers = new HttpHeaders({
         'Content-Type': 'application/json'
     });
     let params = new HttpParams().set('id', id);
     try {
         let response = await this.http.get(url + '/artista/tracks', { headers: headers, params: params })
             .toPromise() as TrackModel[];
             console.log("data de getArtistaTracks --> ",response)
             return response;
     } catch (error) {
         console.log("Error de getArtistaTracks --> ",error);
     }
  }
  
  async getArtistaRelacionado(id: string) {
     console.log("En artistas relacionados")
     let headers = new HttpHeaders({
         'Content-Type': 'application/json'
     });
     let params = new HttpParams().set('id', id);
     try {
         let response = await this.http.get(url + '/artista/relacionado', { headers: headers, params: params })
             .toPromise() as ArtistModel[];
             console.log("data de getArtistaRelacionado --> ",response)
             return response;
     } catch (error) {
         console.log("Error de getArtistaRelacionado --> ",error);
     }
  }
  
  async getArtistaAlbum(id: string) {
     console.log("En album del artista")
     let headers = new HttpHeaders({
         'Content-Type': 'application/json'
     });
     let params = new HttpParams().set('id', id);
     try {
         let response = await this.http.get(url + '/artista/album', { headers: headers, params: params })
             .toPromise() as AlbumModel[];
             console.log("data de getArtistaAlbum --> ",response)
             return response;
     } catch (error) {
         console.log("Error de getArtistaAlbum --> ",error);
     }
  }
  
}

