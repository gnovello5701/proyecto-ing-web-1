import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { PlaylistModel } from '../models/playlist.model';
import { AlbumModel } from '../models/album.model';
import { CategoriaModel } from '../models/categoria.model';


const url = environment.localhost;

@Injectable({
  providedIn: 'root'
})
export class BrowseService {

  loading: boolean;
  error: boolean;

  constructor(private http: HttpClient) {
     this.loading = true;
     this.error = false;
 }

 async getNewReleases() {
     console.log("en get new Releases");
     let headers = new HttpHeaders({
         'Content-Type': 'application/json'
     });
     try {
         const response = await this.http.get(url + '/browse/new-releases', { headers: headers})
         .toPromise() as AlbumModel[];
         console.log("data de getNewReleases-->",response)
         return response;
     } catch (error) {
         console.log("Error en getNewReleases-->",error);
     }
  }
  
  async getPlaylistsDestacadas() {
     console.log("en get playlists destacadas");
     let headers = new HttpHeaders({
         'Content-Type': 'application/json'
     });
     try {
         const response = await this.http.get(url + '/browse/playlist/destacada', { headers: headers})
         .toPromise() as PlaylistModel[];
         console.log("data de getPlaylistsDestacadas-->",response)
         return response;
     } catch (error) {
         console.log("Error en getPlaylistsDestacadas-->",error);
     }
  }
  
  async getBusquedaPlaylistDeUnaCategoria(id:string) {
     console.log("en get playlists de una categoria");
     let headers = new HttpHeaders({
         'Content-Type': 'application/json'
     });
     let params = new HttpParams().set('id', id);
     try {
         const response = await this.http.get(url + '/browse/playlist/categoria', { headers: headers,params: params})
         .toPromise() as PlaylistModel[];
         console.log("data de getBusquedaPlaylistDeUnaCategoria-->",response)
         return response;
     } catch (error) {
         console.log("Error en getBusquedaPlaylistDeUnaCategoria-->",error);
     }
  }
  
  async getBusquedaDeUnaCategoria(id: string) {
     console.log("en get de una categoria");
     let headers = new HttpHeaders({
         'Content-Type': 'application/json'
     });
     let params = new HttpParams().set('id', id);
     try {
         const response = await this.http.get(url + '/browse/categoria', { headers: headers,params: params})
         .toPromise();
         console.log("data de getBusquedaDeUnaCategoria-->",response)
         return response;
     } catch (error) {
         console.log("Error en getBusquedaDeUnaCategoria-->",error);
     }
  }
  
  async getListaBusquedaCategorias() {
     console.log("en get de una lista de categorias");
     let headers = new HttpHeaders({
         'Content-Type': 'application/json'
     });
     try {
         const response = await this.http.get(url + '/browse/categorias', { headers: headers})
         .toPromise() as CategoriaModel[];
         console.log("data de getListaBusquedaCategorias-->",response)
         return response;
     } catch (error) {
         console.log("Error en getListaBusquedaCategorias-->",error);
     }
  }
  
  async getGenerosDisponibles() {
     console.log("en get de una lista de generos");
     let headers = new HttpHeaders({
         'Content-Type': 'application/json'
     });
     try {
         const response = await this.http.get(url + '/browse/generos', { headers: headers})
         .toPromise();
         console.log("data de getGenerosDisponibles-->",response)
         return response;
     } catch (error) {
         console.log("Error en getGenerosDisponibles-->",error);
     }
  }

}