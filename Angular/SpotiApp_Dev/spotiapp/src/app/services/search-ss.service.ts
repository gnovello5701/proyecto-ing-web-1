import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { SearchModel } from '../models/search.model';

const url = environment.localhost;

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }


  async getBuscar(nombre:string,tipo:string) {
    console.log("En getBuscar")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('nombre', nombre).set('tipo',tipo);
    try {
        let response = await this.http.get(url + '/search', { headers: headers, params:params})
            .toPromise() as SearchModel;
            console.log("data de getBuscar-->",response)
            return response;
    } catch (error) {
        console.log("Error en getBuscar",error);
    }
  }
}

