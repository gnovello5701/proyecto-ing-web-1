import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { PlaylistModel } from '../models/playlist.model';
import { TrackModel } from '../models/track.model';

const url = environment.localhost;

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  constructor(private http: HttpClient) { }
  
  async getPlaylist(id:string) {
    console.log("En get playlist")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('id', id);
    try {
        let response = await this.http.get(url + '/playlist', { headers: headers,params:params})
            .toPromise() as PlaylistModel;
            console.log("data de getPlaylist-->",response)
            return response;
    } catch (error) {
        console.log("Error en getPlaylist-->",error);
    }
  }
  
  async getPlaylistTracks(id:string) {
    console.log("En get tracks de la playlist")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('id', id);
    try {
        let response = await this.http.get(url + '/playlist/tracks', { headers: headers,params:params})
        .toPromise() as TrackModel[];
        console.log("data de getPlaylistTracks-->",response)
        return response;
    } catch (error) {
        console.log("Error en getPlaylistTracks-->",error);
    }
  }

  async getListaPlaylistUsuario(id:string) {
    console.log("En get playlists del usuario")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams().set('id', id);
    try {
        let response = await this.http.get(url + '/playlist/usuario', { headers: headers, params:params})
            .toPromise() as PlaylistModel[];
            console.log("data de getListaPlaylistUsuario-->",response)
            return response;
    } catch (error) {
        console.log("Error en getListaPlaylistUsuario-->",error);
    }
  }

  async addTrackToPlaylist(playlistID:string, trackURI:string) {
    console.log("en addTrackToPlaylist")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams()
    .set('playlist_id', playlistID)
    .set('uris',trackURI);

    try {
        let response = await this.http.get(url + '/playlist/addTrack', { headers: headers, params:params})
            .toPromise();
            console.log("data de addTrackToPlaylist-->",response)
            return response;
    } catch (error) {
        console.log("Error en addTrackToPlaylist-->",error);
    }
  }

  async createPlaylist(nombre:string) {
    console.log("en createPlaylist")
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    let params = new HttpParams()
    .set('user_id', "11136960871")
    .set('name', nombre)
    .set('description', "Playlist creada desde SpotiApp")
    .set('public', 'false');

    try {
        let response = await this.http.get(url + '/playlist/crear', { headers: headers, params:params})
            .toPromise();
            console.log("data de createPlaylist-->",response);
            return response;
    } catch (error) {
        console.log("Error en createPlaylist-->",error);
    }
  }
}

