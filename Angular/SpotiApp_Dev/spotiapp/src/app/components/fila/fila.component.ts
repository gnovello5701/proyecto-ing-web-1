import { Component, OnInit, Input } from '@angular/core';
import { ArtistModel } from 'src/app/models/artist.model';
import { AlbumModel } from 'src/app/models/album.model';
import { PlaylistModel } from 'src/app/models/playlist.model';
import { TrackModel } from 'src/app/models/track.model';

@Component({
  selector: 'app-fila',
  templateUrl: './fila.component.html',
  styleUrls: ['./fila.component.css']
})
export class FilaComponent implements OnInit {


  @Input() items:ArtistModel[] | AlbumModel[] | PlaylistModel[] | TrackModel[] = []; 
  @Input() nombreFila:string;
  @Input() descripcion?:string;


  constructor() {
  }

  ngOnInit() {
  }
}
