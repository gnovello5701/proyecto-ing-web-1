import { Component, OnInit, Output,Input, EventEmitter, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AudioService } from 'src/app/services/audio-ss.service';
import { AlbumService } from "./../../../services/album-ss.service";
import { ArtistService } from "./../../../services/artist-ss.service";
import { PlaylistService } from "./../../../services/playlist-ss.service";
import { async } from 'rxjs/internal/scheduler/async';
import { album } from '../../../../assets/mockdata/album';
import { AlbumModel } from 'src/app/models/album.model';
import { ArtistModel } from 'src/app/models/artist.model';
import { PlaylistModel } from 'src/app/models/playlist.model';



@Component({
  selector: 'app-tarjeta-lista',
  templateUrl: './tarjeta-lista.component.html',
  styleUrls: ['./tarjeta-lista.component.css']
})
export class TarjetaListaComponent {

  @Input() item:AlbumModel | ArtistModel | PlaylistModel;
  
  mostrar:boolean = false;
  play:boolean = true;
  estilo:any;

  constructor(private router: Router,
  private audioService: AudioService, 
  private albumService:AlbumService,
  private playlistService:PlaylistService, 
  private artistService:ArtistService) {}




    applyStyles() {
      var  styles:any; 
      if(this.item.type==='artist'){
      styles = {'border-radius' : '300px'};
      }else{
      styles = null;    
      }
     
      return styles;
  }

  ver() {  
      let id = this.item.id;
  
    if(this.item.type==='artist') {
      this.router.navigate(['/artista',id]);
      
    }

    if(this.item.type==='album') {
      this.router.navigate(['album',id]);
    }
    
    if(this.item.type==='playlist') {
      this.router.navigate(['playlist',id]);

      
    }
  }

  async mandarTrack(){

    if(!this.play){
      if(this.item.type==='artist') {      
        this.audioService.clearInfo();
        let track = await this.getArtistsTracks(this.item.id);
        console.log(track);
        this.audioService.sendInfo(track.id); 
      }
      if(this.item.type==='album') {       
        this.audioService.clearInfo();
        let track = await this.getAlbumTracks(this.item.id);
        this.audioService.sendInfo(track.id);
      }     
      if(this.item.type==='playlist') {  
        this.audioService.clearInfo();
        let track = await this.getPlaylistTracks(this.item.id);
        this.audioService.sendInfo(track.id);
      }
    }
  }
 
  mostrarBoton(){    
    if(!this.play){
    this.estilo = {'display' : 'inline-flex'};
    }else{
    this.estilo = null;
    }
  }

  async getAlbumTracks(id:string) {
    let datos = await this.albumService.getAlbumsTracks(id);
    console.log("Este es el item portada de getAlbumTracks>>>>>>>>>>>> artista", datos);
    return datos[0];
  }

  async getPlaylistTracks(id:string) {
    let datos = await this.playlistService.getPlaylist(id);
    console.log("Este es el item portada de getPlaylistTracks>>>>>>>>>>>> artista", datos);
    return datos[0];
  }   
   
  async getArtistsTracks(id:string) {
    let datos = await this.artistService.getArtistaTracks(id);
    console.log("Este es el item portada de getArtistsTracks>>>>>>>>>>>> artista", datos);
    return datos[0];
  }   



}