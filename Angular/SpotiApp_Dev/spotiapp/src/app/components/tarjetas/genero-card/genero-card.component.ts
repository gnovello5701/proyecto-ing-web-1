import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriaModel } from 'src/app/models/categoria.model';

@Component({
  selector: 'app-genero-card',
  templateUrl: './genero-card.component.html',
  styleUrls: ['./genero-card.component.css']
})
export class GeneroCardComponent implements OnInit {

  @Input() item:CategoriaModel;
  color:string;
  color2:string;

  constructor(private router: Router) {
    this.color2 = this.colorHEX()
   }

  ngOnInit() {
  }
  
  verGenero() {
    this.router.navigate(['genero',this.item.id,this.item.name,this.color2,this.item.icon.url]);

  }

  generarLetra(){
    let letras = ["a","b","c","d","e","f","0","1","2","3","4","5","6","7","8","9"];
    let numero = (Math.random()*15).toFixed(0);
    return letras[numero];
  }
    
  colorHEX(){
    this.color = "";
    for(let i=0;i<6;i++){
      this.color = this.color + this.generarLetra() ;
    }
    return "#" + this.color;
  }

  applyStyles() {
    const styles = {'background-color' : this.color2};
    
    return styles;
}
}
