import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ArtistService } from '../../../services/artist-ss.service';
import { AudioService } from 'src/app/services/audio-ss.service';
import { ArtistModel } from 'src/app/models/artist.model';
import { AlbumModel } from 'src/app/models/album.model';
import { TrackModel } from 'src/app/models/track.model';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styleUrls: ['./artista.component.css']
})
export class ArtistaComponent implements OnInit {

  id:string;
  items:any[] = [];
  portada:string;
  nombre:string;
  artistas:ArtistModel[] = [];
  itemsAlbum:AlbumModel[] = [];
  tracks:TrackModel[] = [];
  seccion:boolean = true;
  fondo:any;
  loading:boolean;
  artista:ArtistModel;
  
    
  constructor(private activatedRoute:ActivatedRoute,
              private spotifiArtista:ArtistService,
              private router: Router,
              private audioService: AudioService){

      this.loading = true;
    this.activatedRoute.params.subscribe(params =>{ 
      this.id = params.id;
      this.getArtista(); 
      this.getArtistaAlbum()
      this.getTrackss()
      this.getArtistasRelaciondos();
      this.seccion = true;
   });
    
    
  }

  general() {
    if(this.seccion){
    return  {'color' : 'white'};
    }else{
    return null;    
    }
  }

  otros() {
    if(!this.seccion){
    return  {'color' : 'white'};
    }else{
    return null;    
    }
  }
  
  sendInf() {
    this.audioService.sendInfo(this.tracks[0].id);
  }

  async getArtistaAlbum() {
    this.itemsAlbum = await this.spotifiArtista.getArtistaAlbum(this.id);
    console.log("Este es el item de getArtistaAlbum>>>>>>>>>>>> artista", this.itemsAlbum);
   }
 
 
   async getTrackss() {
    this.tracks = await this.spotifiArtista.getArtistaTracks(this.id);
     console.log("Este es el item de getTrackss>>>>>>>>>>>> artista", this.tracks);
    
   }
  
  async getArtista() {
    this.artista = await this.spotifiArtista.getArtista(this.id);
    console.log("Este es el item portada de getArtista>>>>>>>>>>>> artista", this.artista);
    this.portada = this.artista.images.url;
    this.nombre = this.artista.name;
   }

   async getArtistasRelaciondos() {
    this.artistas = await this.spotifiArtista.getArtistaRelacionado(this.id);
    console.log("Este es el item portada de getArtistasRelaciondos>>>>>>>>>>>> artista", this.artistas);
    this.loading = false;
  }


  ngOnInit() {}

}
