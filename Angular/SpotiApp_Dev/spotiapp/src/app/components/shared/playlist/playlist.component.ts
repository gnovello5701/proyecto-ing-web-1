import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { PlaylistService } from 'src/app/services/playlist-ss.service';
import Swal from "sweetalert2";
import { AudioService } from 'src/app/services/audio-ss.service';
import { TrackService } from "./../../../services/track-ss.service";
import { TrackModel } from 'src/app/models/track.model';
import { PlaylistModel } from 'src/app/models/playlist.model';




@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {

  reproducir:boolean = true;
  boton:boolean
  loading:boolean;
  id:string;
  tracks:TrackModel[] = [];
  item:PlaylistModel;
  ids:string = " ";
  usuario:boolean;

  constructor(private activatedRoute:ActivatedRoute,
  private playlistService:PlaylistService,
  private audioService:AudioService,private trackService:TrackService){

    this.loading = true;
      this.activatedRoute.params.subscribe(params =>{ 
        this.id = params.id;
        this.getPlaylist(this.id);
      });

   this.getPlaylist(this.id);
   
  }

  async getPlaylist(id:string) {
    this.item = await this.playlistService.getPlaylist(id);
    this.tracks = this.item.tracks;
    this.loading = false;
    }

  
    sendInfo(){
      if(!this.reproducir){
        this.audioService.sendInfo(this.tracks[0].id);
      }
    }

    sendInf() {
      this.audioService.sendInfo(this.tracks[0].id);
    }
   
  ngOnInit() {


  }

}
