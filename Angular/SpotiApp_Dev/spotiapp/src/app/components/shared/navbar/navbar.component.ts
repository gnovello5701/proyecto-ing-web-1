import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlaylistService } from 'src/app/services/playlist-ss.service';
import { PlaylistModel } from 'src/app/models/playlist.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  nombre:string;
  playlists:PlaylistModel[] = [];

  constructor(private spotify:PlaylistService,
    private router: Router) {
      this.llenarListPlaylist();
     }

  ngOnInit() {
  }
  async llenarListPlaylist() {
    this.playlists = await this.spotify.getListaPlaylistUsuario("11136960871");
  }

  home(){
    this.router.navigate(['home']);
  }

  instalar(){
  let urlToOpen="https://www.spotify.com/ar/download/linux";
    let url: string = '';
    if (!/^http[s]?:\/\//.test(urlToOpen)) {
      url += 'http://';
    }
      url += urlToOpen;
    window.open(url, '_blank');
  }


  crearPlaylist(){
    this.router.navigate(['crearPlaylist']);
  }
}
