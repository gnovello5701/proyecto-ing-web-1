import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { BrowseService } from 'src/app/services/browse-ss.service';
import { playlist } from '../../../../assets/mockdata/playlist';
import { PlaylistModel } from 'src/app/models/playlist.model';

@Component({
  selector: 'app-genero',
  templateUrl: './genero.component.html',
  styleUrls: ['./genero.component.css']
})
export class GeneroComponent implements OnInit {

nombre:string;
id:string;
playlists:PlaylistModel[] = [];
color:any;
foto:any;

  constructor(private activatedRoute:ActivatedRoute,private browseService:BrowseService) { 
    this.activatedRoute.params.subscribe(params =>{ 
      this.id = params.id;
      this.nombre = params.name;
      this.color = params.color;
      this.foto = params.foto;
  });

  this.getPlaylist(this.id)
  }



  async getPlaylist(id:string) {
    this.playlists = await this.browseService.getBusquedaPlaylistDeUnaCategoria(id);
  }

  applyStyles() {
    const styles = {'background-color' : this.color};
    return styles;
}



  ngOnInit() {
  }

}
