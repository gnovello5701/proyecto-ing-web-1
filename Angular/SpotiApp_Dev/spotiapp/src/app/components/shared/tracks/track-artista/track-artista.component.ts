import { Component, OnInit,Input } from '@angular/core';
import { PlaylistService } from 'src/app/services/playlist-ss.service';
import { AlbumService } from 'src/app/services/album-ss.service';
import { AudioService } from 'src/app/services/audio-ss.service';
import { TrackModel } from 'src/app/models/track.model';


@Component({
  selector: 'app-track-artista',
  templateUrl: './track-artista.component.html',
  styleUrls: ['./track-artista.component.css']
})
export class TrackArtistaComponent implements OnInit {

  @Input() track:TrackModel;

  play:boolean = false; 

  
  constructor(private audioService:AudioService) {   


  }

  ngOnInit() {

  } 

  sendInfo():void {
    this.audioService.sendInfo(this.track.id);
  }

  clearInfo(): void {
    this.audioService.clearInfo();
  }

  space() {
   let separador:string = ".";
    return separador;
  }

  getNames() {
    let artists:any[]= this.track.artistas;
    let stringArtists:string = "";

    for (let i = 0; i < artists.length; i++) {
      let nombre = artists[i].name;
      stringArtists = stringArtists.concat( nombre + " ");
    }
    stringArtists.replace(/.$/,"."); //trato de reemplazar el ultimo caracter
    return stringArtists;
  }

  
  trackLegenth(milis:number) {
    let segundos:string
     var ms = 1000*Math.round(milis/1000);
     var d = new Date(ms);
     let aux = d.getUTCSeconds()
     if(aux < 10){
       segundos = "0" + aux;
     }else{
       segundos = "" +aux;
     }
     return ( d.getUTCMinutes() + ':' + segundos ); 
   }

}
