import { Component, OnInit } from '@angular/core';
import { AlbumService } from 'src/app/services/album-ss.service';
import { ActivatedRoute } from "@angular/router";
import { AudioService } from 'src/app/services/audio-ss.service';
import Swal from "sweetalert2";
import { AlbumModel } from 'src/app/models/album.model';
import { TrackModel } from 'src/app/models/track.model';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {

  loading:boolean;
  id:string;
  tracks:TrackModel[] = [];
  item:AlbumModel;
  reproducir:boolean = true;
  boton:boolean

  constructor(private activatedRoute:ActivatedRoute,
  private albumService:AlbumService,private audioService:AudioService){

  this.loading = true;
  this.activatedRoute.params.subscribe(params =>{ 
  this.id = params.id;
  this.getAlbum(this.id);
  });
  this.getAlbum(this.id);
  
  
  }
  
    ngOnInit() {
    }
  
    async getAlbum(id:string) {
      this.item = await this.albumService.getAlbum(id);
      console.log(this.item);
      this.tracks = await this.albumService.getAlbumsTracks(id);
      this.loading = false;
    }
  
  
  sendInfo(){
  
  if(!this.reproducir){
    this.audioService.sendInfo(this.tracks[0].id);
  }
}
  sendInf() {
    this.audioService.sendInfo(this.tracks[0].id);
  }

  
}
