import { Component, OnInit } from '@angular/core';
import { TrackService } from 'src/app/services/track-ss.service';
import { AudioService } from 'src/app/services/audio-ss.service';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  info: any[] = [];
  subscription : Subscription;

  files: Array<any> = [];
  state: any;
  currentFile: any = {};

  item:any;
  itemPrueba:any;
  nombreTrack:string = "";
  textoArriva:string;
  nombreAbajo:string;
  urlImage:string;
  duracion:string;
  srcAudio:string;

  constructor(private spotify:TrackService,
              private audioService:AudioService) {

   this.setPrimerTrack();
      
    this.subscription = this.audioService.getInfo().subscribe(data => {
      if(data) {
        console.log("Lo que llega al footer por AudioService>>>> ",data);
        this.info.push(data.data);
        this.setUnTrack(data.data);
      } else {
        this.info = [];
      }
    });

    

  }

  ngOnInit() { 
  }

  async setPrimerTrack() {
    this.itemPrueba = await this.spotify.getTrack('7z9chavxReUpyOZoAkUnUb');
    this.files.push(this.itemPrueba);
    this.nombreTrack = this.itemPrueba.name;
    this.urlImage = this.itemPrueba.album.images[0].url;
    this.nombreAbajo = this.itemPrueba.artists[0].name;
  }

  async setUnTrack(id:string) {
    this.itemPrueba = await this.spotify.getTrack(id);
    this.files.push(this.itemPrueba);
    this.nombreTrack = this.itemPrueba.name;
    if(this.itemPrueba.preview_url===null) {
    let timerInterval
    Swal.fire({
      showClass: {
        popup: 'animated fadeInDown faster'
      },
      hideClass: {
        popup: 'animated fadeOutUp faster'
      },
      title: 'Ups! Este track no tiene audio preview.',
      timer: 1600,
      onBeforeOpen: () => {
        Swal.DismissReason.cancel,
        timerInterval = setInterval(() => {
          const content = Swal.getContent()
          if (content) {
            const b = content.querySelector('b')
          }
        }, 100)
      },
      onClose: () => {
        clearInterval(timerInterval)
      }
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.dismiss === Swal.DismissReason.timer) {
        console.log('I was closed by the timer')
      }
    })
    }
    
    this.srcAudio = this.itemPrueba.preview_url;
    this.urlImage = this.itemPrueba.album.images.url;
    this.nombreAbajo = this.itemPrueba.artists[0].name;
  }

  trackLegenth(milis:number) {
    var ms = 1000*Math.round(milis/1000);
    var d = new Date(ms);
    return ( d.getUTCMinutes() + ':' + d.getUTCSeconds() ); 
  }
  
  playStream(url) {
    this.audioService.playStream(url).subscribe(events => {
      // listening for fun here
    });
  }
  
  isFirstPlaying() {
    return this.currentFile.index === 0;
  }

  isLastPlaying() {
    return this.currentFile.index === this.files.length - 1;
  }

  openFile(file, index) {
    this.currentFile = { index, file };
    this.audioService.stop();
    this.playStream(file.url);
  }

  pause() {
    this.audioService.pause();
  }
  play() {
    this.audioService.play();
  }

  stop() {
    this.audioService.stop();
  }

  next() {
    const index = this.currentFile.index + 1;
    const file = this.files[index];
    this.openFile(file, index);
  }
  previous() {
    const index = this.currentFile.index - 1;
    const file = this.files[index];
    this.openFile(file, index);
  }



}
