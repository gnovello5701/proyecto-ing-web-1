import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { PlaylistService } from 'src/app/services/playlist-ss.service';

@Component({
  selector: 'app-crear-playlist',
  templateUrl: './crear-playlist.component.html',
  styleUrls: ['./crear-playlist.component.css']
})
export class CrearPlaylistComponent implements OnInit {

  nombrePlaylist:string;
  cruz:boolean = true
  constructor(private locacion:Location,
    private playlistService:PlaylistService) { }

  volver(){
    this.locacion.back();
  }

  textoIngresado(texto:string){
    this.nombrePlaylist = texto;
  }

  async crear(){
    let respuesta = await this.playlistService.createPlaylist(this.nombrePlaylist);
    console.log(respuesta);
  }

  ngOnInit() {
  }

}
