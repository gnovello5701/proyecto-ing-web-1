import { Component, OnInit } from '@angular/core';
import { BrowseService } from 'src/app/services/browse-ss.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AlbumService } from "../../services/album-ss.service";
import { AlbumModel } from 'src/app/models/album.model';
import { PlaylistModel } from 'src/app/models/playlist.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  itemsNewReleases:AlbumModel[] = [];
  itemsPlaylistDest:PlaylistModel[]= [];
  loading:boolean;
  nombreNewReleases:string = "";
  nombrePlaylistDest:string = "";
  
  constructor( private spotifyBrowse : BrowseService,
               public auth : AuthService,
               ) {

  }
  async setItemsNewReleases() {
    this.itemsNewReleases = await this.spotifyBrowse.getNewReleases();
    console.log("Este es el item de setItemsNewReleases>>>>>>>>> home", this.itemsNewReleases);
    this.nombreNewReleases = "Ultimos Lanzamientos";
  }
  async setPlaylistDestacadas() {
    this.itemsPlaylistDest = await this.spotifyBrowse.getPlaylistsDestacadas();
    console.log("Este es el item de setPlaylistDestacadas>>>>>>>>> home", this.itemsPlaylistDest);
    this.nombrePlaylistDest = "Playlist Destacadas"

  }
  ngOnInit() {
    this.loading = true;
    this.setItemsNewReleases();
    this.setPlaylistDestacadas();
    this.loading = false;
  }


}
