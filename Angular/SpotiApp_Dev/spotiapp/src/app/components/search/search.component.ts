import { Component } from '@angular/core';
import { BrowseService } from 'src/app/services/browse-ss.service';
import { SearchService } from '../../services/search-ss.service';
import { ArtistService } from '../../services/artist-ss.service';
import { AlbumService } from "../../services/album-ss.service"
import { PlaylistService } from "./../../services/playlist-ss.service";
import { Router, ActivatedRoute } from '@angular/router';
import { AudioService } from 'src/app/services/audio-ss.service';

import { PlaylistModel} from '../../models/playlist.model';
import { AlbumModel } from 'src/app/models/album.model';
import { ArtistModel } from 'src/app/models/artist.model';
import { TrackModel } from 'src/app/models/track.model';
import { SearchModel } from 'src/app/models/search.model';
import { CategoriaModel } from 'src/app/models/categoria.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
  
})
export class SearchComponent {
  estilo:any;
  loading: boolean;
  generos:any[] = [];
  nombresGeneros:string;
  nombreFilaG:string;
  playlists:PlaylistModel[] = [];
  albums:AlbumModel[] = []
  artistas:ArtistModel[] = [];
  mostrar:boolean;
  nombreFila:string;
  primerArtista:string;
  primerFoto:any;
  track:TrackModel[] = [];
  tracksArtista:TrackModel[] = [];
  play:boolean = true;

  constructor(private spotifyBrowse: BrowseService,
  private spotifySearch:SearchService,private spotifyArtist:ArtistService,
  private spotifyAlbum:AlbumService,private playlistService:PlaylistService,
  private router:Router, private audioService: AudioService) {

    this.setItemsGeneros();
   }

  buscar(termino: string) {
   
  if(termino.trim() === ""){
    this.mostrar = false;
    this.loading = false;
  }else{

  this.loading = true;
  this.searchArtist(termino);
  this.searchAlbum(termino);
  this.searchPlaylist(termino);

  this.mostrar = true;

  setTimeout(()=>{this.primeraImpresion()}, 3000);
  this.primeraImpresion();

  }

   
  }  


async primeraImpresion(){

this.loading = true;
this.primerFoto = this.artistas[0].images;
this.primerArtista = this.artistas[0].name;
this.track = await this.primerosTracks(this.artistas[0].id)
this.loading = false;
}

verArtista(){
  this.router.navigate(['artista',this.artistas[0].id]);
}


  async searchPlaylist(letra:string) {
    let item:SearchModel = await this.spotifySearch.getBuscar(letra,"playlist");
    console.log("ESTE ES EL ITEM PLAYLIST", item.playlists);
    this.playlists = item.playlists;
  }

  async searchAlbum(letra:string) {
    let item:SearchModel = await this.spotifySearch.getBuscar(letra,"album");
    console.log("ESTE ES EL ITEM ALBUM", item.albums);
    this.albums = item.albums;
  }

  async searchArtist(letra:string) {
    let item:SearchModel = await this.spotifySearch.getBuscar(letra,"artist");
    console.log("ESTE ES EL ITEM ARTIST", item.artists);
    this.artistas = item.artists;
  }

  async primerosTracks(id:string) {
    let item:TrackModel[] = await this.spotifyArtist.getArtistaTracks(id);
    return item;
  }



  async setItemsGeneros() {
    this.generos = await this.spotifyBrowse.getListaBusquedaCategorias();
    console.log("este es el item de setItemsGeneros>>>>>>>>>>> search", this.generos);
  }


  hideText() {
    var text = document.getElementById("textoInp");
    if (text.style.display === "inline") {
      text.style.display = "none";
    } else {
      text.style.display = "inline";
    }
  }

  mostrarBoton(){    
    if(!this.play){
    this.estilo = {'display' : 'inline-flex'};
    }else{
    this.estilo = null;
    }
  }

  async mandarTrack(){
    if(!this.play){     
        this.audioService.clearInfo();
        this.audioService.sendInfo(this.track[0].id);
      
    }
  }

}
