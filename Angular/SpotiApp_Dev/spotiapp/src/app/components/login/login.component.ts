import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import Swal from "sweetalert2";

import { UsuarioModel } from '../../models/usuario.model';
import { AuthService } from '../../services/auth/auth.service';
import { LoginService } from 'src/app/services/login.service';
import { AlbumService } from 'src/app/services/album-ss.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel = new UsuarioModel();
  public recordarme:boolean = false;

  constructor( private auth: AuthService,
               private router: Router,
               private log:LoginService,
               private servicioPrueba:AlbumService ) { }



  ngOnInit() {

    if ( localStorage.getItem('email') ) {
      this.usuario.email = localStorage.getItem('email');
      this.recordarme = true;
    }

  }

  redirect():void {
    this.router.navigate(['register']);
  }


  login( form: NgForm ) {

    if (  form.invalid ) { return; }

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();

    this.auth.login( this.usuario )
      .subscribe( resp => {

        console.log(resp);
        Swal.close();

        if ( this.recordarme ) {
          localStorage.setItem('email', this.usuario.email);
        }

        
        this.logg();
        this.router.navigate(['home']);
        // this.router.navigateByUrl('/home');

      }, (err) => {

        console.log(err.error.error.message);
        Swal.fire({
          icon: 'error',
          title: 'Error al autenticar',
          text: err.error.error.message
        });
      });

  }

  async logg() {
    let respuesta =  await this.log.logins();
    console.log("esta es la respuesta: ",respuesta);
  }
}
