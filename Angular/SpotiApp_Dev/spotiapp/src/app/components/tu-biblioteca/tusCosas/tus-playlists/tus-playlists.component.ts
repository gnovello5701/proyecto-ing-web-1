import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { PlaylistService } from "../../../../services/playlist-ss.service";
import { PlaylistModel } from 'src/app/models/playlist.model';

@Component({
  selector: 'app-tus-playlists',
  templateUrl: './tus-playlists.component.html',
  styleUrls: ['./tus-playlists.component.css']
})
export class TusPlaylistsComponent implements OnInit {


playlists:PlaylistModel[] = [];

constructor(private spotify:PlaylistService) {
  this.usuarioPlaylist();
}
 


async usuarioPlaylist(){
  this.playlists = await this.spotify.getListaPlaylistUsuario("11136960871");
}
ngOnInit() {}
}