import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tu-biblioteca',
  templateUrl: './tu-biblioteca.component.html',
  styleUrls: ['./tu-biblioteca.component.css']
})
export class TuBibliotecaComponent implements OnInit {

playlist:any;

 constructor(private router: Router) {
  this.router.navigate(['biblioteca/misPlaylists']);
  
  
 }
  


  ngOnInit() {
    this.router.navigate(['biblioteca/misPlaylists']);
  }

}
