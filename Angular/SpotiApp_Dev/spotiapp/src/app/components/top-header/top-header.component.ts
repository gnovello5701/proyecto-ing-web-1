import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-header',
  templateUrl: './top-header.component.html',
  styleUrls: ['./top-header.component.css']
})
export class TopHeaderComponent implements OnInit {
  


  constructor(private locacion:Location,
    public auth:AuthService,
    private router:Router) { 

  }

  ngOnInit() {
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['login']);
  }

  autenticado():boolean {
    if(this.auth.estaAutenticado()) {
      return true;
    } else {
      return false;
    }
  }

  volver(){
    this.locacion.back();
  }


  ir(){
    this.locacion.forward();
  }


}
