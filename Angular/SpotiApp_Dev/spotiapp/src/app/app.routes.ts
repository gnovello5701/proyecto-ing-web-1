import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { TuBibliotecaComponent } from './components/tu-biblioteca/tu-biblioteca.component';
import { PlaylistComponent } from './components/shared/playlist/playlist.component';
import { ArtistaComponent } from './components/shared/artista/artista.component';
import { TusAlbumsComponent } from "./components/tu-biblioteca/tusCosas/tus-albums/tus-albums.component";
import { TusArtistasComponent } from "./components/tu-biblioteca/tusCosas/tus-artistas/tus-artistas.component";
import { TusPlaylistsComponent } from "./components/tu-biblioteca/tusCosas/tus-playlists/tus-playlists.component";
import { AuthGuard } from './services/guards/auth.guard';
import { GeneroComponent } from './components/shared/genero/genero.component';
import { AlbumComponent } from "./components/shared/album/album.component";
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { PlayerComponent } from './components/player/player.component';
import { CrearPlaylistComponent } from './components/crear-playlist/crear-playlist.component';

export const ROUTES: Routes = [
    

    { path: 'register', component: RegisterComponent},
    { path: 'login', component: LoginComponent},
    { path: 'player', component: PlayerComponent},
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
    { path: 'crearPlaylist', component: CrearPlaylistComponent, canActivate: [AuthGuard]},
    { path: 'search', component: SearchComponent, canActivate: [AuthGuard]}, 
    { path: 'genero/:id/:name/:color/:foto', component: GeneroComponent, canActivate: [AuthGuard]},
    { path: 'playlist/:id', component: PlaylistComponent, canActivate: [AuthGuard]},
    { path: 'album/:id', component: AlbumComponent, canActivate: [AuthGuard]},
    { path: 'artista/:id', component: ArtistaComponent, canActivate: [AuthGuard]},
    { path: 'biblioteca', component: TuBibliotecaComponent, canActivate: [AuthGuard],
      children:[
        { path: 'misAlbums/:id',  component: TusAlbumsComponent, canActivate: [AuthGuard]}, 
        { path: 'misPlaylists',  component: TusPlaylistsComponent, canActivate: [AuthGuard]},
        { path: 'misArtistas/:id',  component: TusArtistasComponent, canActivate: [AuthGuard]},
      ]},

    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

@NgModule({
    imports: [RouterModule.forRoot(ROUTES)],
    exports: [RouterModule]
    
})
export class AppRoutes{};