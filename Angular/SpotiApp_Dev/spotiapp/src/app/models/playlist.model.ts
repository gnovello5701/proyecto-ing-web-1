import { ImageModel } from "./image.model";
import { TrackModel } from "./track.model";

export interface PlaylistModel{
   id:string;
   name:string;
   type:string;
   tracks:TrackModel[];
   images:ImageModel;
}