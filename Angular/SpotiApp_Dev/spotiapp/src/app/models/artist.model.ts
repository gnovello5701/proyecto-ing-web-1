import { CategoriaModel } from './categoria.model';
import { ImageModel } from './image.model';

export interface ArtistModel{
    id:string;
    name:string;
    type:string;
    genres:CategoriaModel[];
    images:ImageModel;
    popularity:number;
}