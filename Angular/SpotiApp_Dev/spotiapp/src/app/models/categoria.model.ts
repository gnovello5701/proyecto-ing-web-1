import { ImageModel } from './image.model';

export interface CategoriaModel{
    id:string;
    name:string;
    type:string;
    icon:ImageModel;
}