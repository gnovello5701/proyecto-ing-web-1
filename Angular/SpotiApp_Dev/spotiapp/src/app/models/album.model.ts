import { ImageModel } from "./image.model";
import { ArtistModel } from "./artist.model";

export interface AlbumModel{
    id:string;
    name:string;
    type:string;
    total_tracks:number;
    images:ImageModel;
    artists:ArtistModel[];
}