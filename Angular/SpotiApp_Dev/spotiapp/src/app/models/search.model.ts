import { ArtistModel } from './artist.model';
import { TrackModel } from './track.model';
import { PlaylistModel } from './playlist.model';
import { AlbumModel } from './album.model';


export interface SearchModel{
    artists:ArtistModel[];
    tracks:TrackModel[];
    playlists:PlaylistModel[];
    albums:AlbumModel[];
}