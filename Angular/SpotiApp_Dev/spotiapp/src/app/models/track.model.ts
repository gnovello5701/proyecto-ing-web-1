import { ArtistModel } from './artist.model';
import { ImageModel } from './image.model';
import { AlbumModel } from './album.model';

export interface TrackModel {
   id:string;
   name:string;
   type:string;
   artistas:ArtistModel[];
   album:AlbumModel;
   preview_url:string;
   duration_ms:number;
   popularity:number;
   images:ImageModel;
}