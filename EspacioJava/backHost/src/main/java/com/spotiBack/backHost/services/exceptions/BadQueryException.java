package com.spotiBack.backHost.services.exceptions;

public class BadQueryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadQueryException() {
		super();
		System.out.println(getRespuesta());
	}
	
	public BadQueryException(String mensaje) {
		super();
		System.out.println(mensaje);
	}
	
	private String getRespuesta() {
		return "La query esta vacia para realizar la peticion";
	}
}
