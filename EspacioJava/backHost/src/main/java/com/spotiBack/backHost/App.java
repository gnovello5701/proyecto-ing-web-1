package com.spotiBack.backHost;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import com.spotiBack.backHost.models.album.AlbumRoutes;
import com.spotiBack.backHost.models.artist.ArtistRoutes;
import com.spotiBack.backHost.models.browse.BrowseRoutes;
import com.spotiBack.backHost.models.playlist.PlayListRoutes;
import com.spotiBack.backHost.models.search.SearchRoutes;
import com.spotiBack.backHost.models.track.TrackRoutes;
import com.spotiBack.backHost.services.FromJsonToObject;
import com.spotiBack.backHost.services.Peticion;
import com.spotiBack.backHost.services.exceptions.BadQueryException;
import com.spotiBack.backHost.services.exceptions.ResponseCodeException;
import com.spotiBack.backHost.util.Token;

import io.javalin.Javalin;
import io.javalin.http.Context;

public class App {

	public static void main(String[] args) throws Exception {

		Javalin app = Javalin.create(config -> {
			config.enableCorsForAllOrigins();
		}).start(7775);

		Token tok = new Token();
		try {
			tok.tokenMantenimiento();
		} catch (Exception e1) {
			System.out.println("Problemas del mantenimiento del token");
			e1.printStackTrace();
		}

		app.get("loguin", ctx -> {
			Peticion peticion = new Peticion();
			String resultado = new String(peticion.login());
			System.out.println(resultado);
			ctx = ctx.json(resultado);
		});

		// ALBUM///////////////////////////////////////////////////////////////////////////////////////////////////////////

		app.get("album/tracks", ctx -> {
			ctx.json(new FromJsonToObject()
					.setAlbumTracks(parseJsonGETOBJECT(AlbumRoutes.getAlbumsTracks(ctx.req.getParameter("id")))));
		});

		app.get("album", ctx -> {
			ctx.json(new FromJsonToObject()
					.setAlbum(parseJsonGETOBJECT(AlbumRoutes.getAnAlbum(ctx.req.getParameter("id")))));
		});

		app.get("album/varios", ctx -> {
			ctx.json(new FromJsonToObject()
					.setVariosAlbums(parseJsonGETOBJECT(AlbumRoutes.getSeveralAlbums(ctx.req.getParameter("id")))));
		});

		// ARTISTAS///////////////////////////////////////////////////////////////////////////////////////////////////////

		app.get("/artista", ctx -> {
			ctx.json(new FromJsonToObject()
					.setArtist(parseJsonGETOBJECT(ArtistRoutes.getArtist(ctx.req.getParameter("id")))));
		});

		app.get("artista/varios", ctx -> {
			ctx.json(new FromJsonToObject()
					.setVariosArtistas(parseJsonGETOBJECT(ArtistRoutes.getVariosArtist(ctx.req.getParameter("id")))));
		});

		app.get("artista/tracks", ctx -> {
			ctx.json(new FromJsonToObject().setArtistTracks(parseJsonGETOBJECT(ArtistRoutes.getArtisTopTrak(ctx.req.getParameter("id")))));
		});

		app.get("artista/relacionado", ctx -> {
			ctx.json(new FromJsonToObject().setVariosArtistas(
					parseJsonGETOBJECT(ArtistRoutes.getArtistRelacionados(ctx.req.getParameter("id")))));
		});

		app.get("artista/album", ctx -> {
			ctx.json(new FromJsonToObject()
					.setArtistAlbum(parseJsonGETOBJECT(ArtistRoutes.getArtistAlbum(ctx.req.getParameter("id")))));
		});

		// BROWSE/////////////////////////////////////////////////////////////////////////////////////////////////////////////

		app.get("browse/new-releases", ctx -> {
			ctx.json(new FromJsonToObject().setNewReleases(parseJsonGETOBJECT(BrowseRoutes.getNewReleases())));
		});

		app.get("browse/playlist/destacada", ctx -> {
			ctx.json(new FromJsonToObject()
					.setPlaylistDestacadas(parseJsonGETOBJECT(BrowseRoutes.getPlaylistsDestacadas())));
		});

		app.get("browse/playlist/categoria", ctx -> {
			ctx.json(new FromJsonToObject().setPlaylistCategoria(
					parseJsonGETOBJECT(BrowseRoutes.getBusquedaPlaylistDeUnaCategoria(ctx.req.getParameter("id")))));
		});

		app.get("browse/categorias", ctx -> {
			ctx.json(new FromJsonToObject()
					.setListaCategoria(parseJsonGETOBJECT(BrowseRoutes.getListaBusquedaCategorias())));
		});

		// Playlists//////////////////////////////////////////////////////////////////////////////////////////

		app.get("playlist/usuario", ctx -> {
			ctx.json(new FromJsonToObject().setPlaylistUser(
					parseJsonGETOBJECT(PlayListRoutes.getListaPlaylistUsuario(ctx.req.getParameter("id")))));
		});

		app.get("playlist", ctx -> {
			ctx.json(new FromJsonToObject()
					.setPlaylist(parseJsonGETOBJECT(PlayListRoutes.getPlaylist(ctx.req.getParameter("id")))));
		});

		app.get("playlist/tracks", ctx -> {
			ctx.json(new FromJsonToObject().setPlaylistTrack(
					parseJsonGETOBJECT(PlayListRoutes.getPlaylistTracks(ctx.req.getParameter("id")))));
		});

		// Tracks///////////////////////////////////////////////////////////////////////////////////////////////////

		app.get("tracks", ctx -> {
			ctx.json(new FromJsonToObject()
					.setVariosTracks(parseJsonGETOBJECT(TrackRoutes.getVariosTracks(ctx.req.getParameter("id")))));
		});

		app.get("track", ctx -> {
			ctx.json(new FromJsonToObject()
					.setTrack(parseJsonGETOBJECT(TrackRoutes.getTrack(ctx.req.getParameter("id")))));
		});

		// Search///////////////////////////////////////////////////////////////////////////////////////////////////

		app.get("search", ctx -> {
			ctx.json(new FromJsonToObject().setSearch(parseJsonGETOBJECT(SearchRoutes.getBuscar(ctx.req.getParameter("nombre"), ctx.req.getParameter("tipo")))));
		});

		return;
	}
	public static Object parseJsonGET(String ruta, Context ctx) {
		try {
			Peticion peticion = new Peticion();
			String resultado = new String(peticion.GET(ruta));
			JSONObject jsonObject = new JSONObject(resultado);
			return ctx.json(jsonObject.toString());
		} catch (JSONException e) {
			System.err.println("Excepcion. Resultado nulo, no se pudo parsear el Json");
		} catch (BadQueryException | ResponseCodeException e) {
			e.getMessage();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.err.println("Problema de URL mal formada");
		} catch (ConnectException e) {
			System.err.println("Problemas de conexion, revisar server de token");
		} catch (ProtocolException e) {
			e.printStackTrace();
			System.err.println("Prbblemas: ProtocolException");
		} catch (IOException e) {
			System.err.println("Problemas: IOException");
		} catch (Exception e) {

		}
		return null;

	}

	public static JSONObject parseJsonGETOBJECT(String ruta) {
		try {
			Peticion peticion = new Peticion();
			String resultado = new String(peticion.GET(ruta));
			JSONObject jsonObject = new JSONObject(resultado);
			return jsonObject;
		} catch (JSONException e) {
			System.err.println("Excepcion. Resultado nulo, no se pudo parsear el Json");
		} catch (BadQueryException | ResponseCodeException e) {
			e.getMessage();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.err.println("Problema de URL mal formada");
		} catch (ConnectException e) {
			System.err.println("Problemas de conexion, revisar server de token");
		} catch (ProtocolException e) {
			e.printStackTrace();
			System.err.println("Prbblemas: ProtocolException");
		} catch (IOException e) {
			System.err.println("Problemas: IOException");
		} catch (Exception e) {

		}
		return null;

	}

	public static String encodeURIComponent(String component) {
		String result = null;

		try {
			result = URLEncoder.encode(component, "UTF-8").replaceAll("\\%28", "(").replaceAll("\\%29", ")")
					.replaceAll("\\+", "%20").replaceAll("\\%27", "'").replaceAll("\\%21", "!")
					.replaceAll("\\%7E", "~");
		} catch (UnsupportedEncodingException e) {
			result = component;
		}
		return result;
	}

	public String autorizacion() {
		Token tok = new Token();

		return "Bearer " + tok.getToken();
	}

}
