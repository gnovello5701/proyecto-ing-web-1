package com.spotiBack.backHost.models.track;

import com.spotiBack.backHost.models.Item;
import com.spotiBack.backHost.models.album.Album;
import com.spotiBack.backHost.models.artist.Artist;
import com.spotiBack.backHost.models.auxiliares.Imagen;

public class Track extends Item {

	private Artist artistas[];
	private Album album;
	private String preview_url;
	private int duration_ms;
	private int popularity;
	private Imagen images;
	
	public Track() {}
	
	public Track(String id, String name, String type, Artist artistas[],Album album,
			String preview_url, int duration_ms, int popularity, Imagen images) {
		super();
		this.setId(id);
		this.setName(name);
		this.setType(type);
		this.artistas = null;
		if(artistas!=null)
			this.artistas = artistas;
		this.album = null;
		if(album!=null)
			this.album = album;
		this.preview_url = preview_url;
		this.duration_ms = duration_ms;
		this.popularity = popularity;
		this.images = images;
	}
	
	public String[] getAtributos() {
		String atributos[] = new String[9];
		atributos[0] = "id";
		atributos[1] = "name";
		atributos[2] = "type";
		atributos[3] = "imagen";
		atributos[4] = "preview_url";//se obtiene de adentro del album
		atributos[5] = "album";
		atributos[6] = "artists";
		atributos[7] = "duration_ms";
		atributos[8] = "popularity";
		return atributos;
	}

	public Artist[] getArtistas() {
		return artistas;
	}

	public void setArtistas(Artist[] artistas) {
		this.artistas = artistas;
	}

	public String getPreview_url() {
		return preview_url;
	}

	public void setPreview_url(String preview_url) {
		this.preview_url = preview_url;
	}

	public int getDuration_ms() {
		return duration_ms;
	}

	public void setDuration_ms(int duration_ms) {
		this.duration_ms = duration_ms;
	}

	public int getPopularity() {
		return popularity;
	}

	public void setPopularity(int popularity) {
		this.popularity = popularity;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public Imagen getImages() {
		return images;
	}

	public void setImages(Imagen images) {
		this.images = images;
	}
	
	

}
