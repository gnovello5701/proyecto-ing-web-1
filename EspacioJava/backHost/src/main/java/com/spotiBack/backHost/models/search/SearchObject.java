package com.spotiBack.backHost.models.search;

import com.spotiBack.backHost.models.album.Album;
import com.spotiBack.backHost.models.artist.Artist;
import com.spotiBack.backHost.models.playlist.Playlist;
import com.spotiBack.backHost.models.track.Track;

public class SearchObject {
	
	private Artist artists[];
	private Track tracks[];
	private Playlist playlists[];
	private Album albums[];
	
	public SearchObject() {}
	
	public SearchObject(Artist artists[], Track tracks[], Playlist playlists[], Album albums[]) {
		this.artists=null;
		if(artists!=null)
			this.artists = artists;
		this.tracks = null;
		if(tracks!=null)
			this.tracks = tracks;
		this.playlists = null;
		if(playlists!=null)
			this.playlists = playlists;
		this.albums = null;
		if(albums!=null)
			this.albums = albums;
	}
	
	public SearchObject(Artist artists[], Playlist playlists[], Album albums[]) {
		this.artists=null;
		if(artists!=null)
			this.artists = artists;
		this.playlists = null;
		if(playlists!=null)
			this.playlists = playlists;
		this.albums = null;
		if(albums!=null)
			this.albums = albums;
	}
	
	public String[] getAtributos() {
		String atributos[] = new String[4];
		atributos[0] = "artists";
		atributos[1] = "tracks";
		atributos[2] = "playlists";
		atributos[3] = "albums";
		return atributos;
	}

	public Artist[] getArtists() {
		return artists;
	}

	public void setArtists(Artist[] artists) {
		this.artists = artists;
	}

	public Track[] getTracks() {
		return tracks;
	}

	public void setTracks(Track[] tracks) {
		this.tracks = tracks;
	}

	public Playlist[] getPlaylists() {
		return playlists;
	}

	public void setPlaylists(Playlist[] playlists) {
		this.playlists = playlists;
	}

	public Album[] getAlbums() {
		return albums;
	}

	public void setAlbums(Album[] albums) {
		this.albums = albums;
	}
	
	

}
