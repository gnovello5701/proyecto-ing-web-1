package com.spotiBack.backHost.util.exceptions;

public class ParseJsonException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ParseJsonException() {
		super();
		System.out.println("No se puede parsear una respuesta vacia");
	}

}
