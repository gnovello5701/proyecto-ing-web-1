package com.spotiBack.backHost.models.album;

public class AlbumRoutes {

	/**
	 * 
	 * @param id : obligatorio
	 * @return String de ruta completa
	 */
	public static String getAlbumsTracks(String id) {
		if (id == null) {
			return null;
		} else {
			return "albums/" + id + "/tracks?offset=0&limit=8&market=AR";
		}

	}

	/**
	 * 
	 * @param id obligatorio
	 * @return String con ruta
	 */

	public static String getAnAlbum(String id) {
		if (id == null) {
			return null;
		} else {
			return "albums/" + id + "?market=AR";
		}
	}

	/**
	 * 
	 * @param ids ids separados por %
	 * @return
	 */

	public static String getSeveralAlbums(String ids) {
		if (ids == null) {
			return null;
		} else {
			return "albums?ids=" + ids + "&market=AR";
		}

	}

}
