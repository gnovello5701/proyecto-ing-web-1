package com.spotiBack.backHost.services;

import java.util.ArrayList;

import com.spotiBack.backHost.services.exceptions.BadQueryException;
import com.spotiBack.backHost.services.exceptions.ResponseCodeException;

public interface IPeticiones {

	public String GET(String query) throws ResponseCodeException, BadQueryException, Exception;

	public String POST(String query, ArrayList<String> list) throws ResponseCodeException, BadQueryException, Exception;

	public String PUT(String query) throws ResponseCodeException, BadQueryException, Exception;

	public String getToken() throws ResponseCodeException, Exception;

	public String login() throws ResponseCodeException, Exception;
}
