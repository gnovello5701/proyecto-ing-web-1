package com.spotiBack.backHost.models.browse;

import com.spotiBack.backHost.models.Item;
import com.spotiBack.backHost.models.auxiliares.Imagen;

public class Categoria extends Item {
	
	private Imagen icons;
	
	public Categoria() {}
	
	public Categoria(String id, String name, Imagen icon) {
		this.setId(id);
		this.setName(name);
		this.setType(null);
		this.icons = null;
		if(icon!=null)
			this.icons = icon;
	}
	
	public String[] getAtributos() {
		String atributos[] = new String[4];
		atributos[0] = "id";
		atributos[1] = "name";
		atributos[2] = "type";
		atributos[3] = "icons";//VER ACA NO ES ASI
		return atributos;
	}

	public Imagen getIcon() {
		return icons;
	}

	public void setIcon(Imagen icons) {
		this.icons = icons;
	}
	
	
}
