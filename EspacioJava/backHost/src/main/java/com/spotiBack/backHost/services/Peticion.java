package com.spotiBack.backHost.services;

import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.eclipse.jetty.server.Response;

import com.spotiBack.backHost.services.exceptions.BadQueryException;
import com.spotiBack.backHost.services.exceptions.ResponseCodeException;
import com.spotiBack.backHost.util.Token;

public class Peticion implements IPeticiones {

	public HttpURLConnection con;
	private int responseCode;
	private final static String URLprincipal = "https://api.spotify.com/v1/";
	private final static String Propiedad1 = "Authorization";
	private String client_id = "4e08fae57975438294428f1a07d3873c";
	private String client_secret = "bb5fb606030645bdaf1d142506370466";

	public Peticion() {
		super();
	}

	@Override
	public String GET(String query) throws Exception {
		Token tok = new Token();
		String autorizacion = "Bearer " + tok.getToken();

		if (query == null)
			throw new BadQueryException();

		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			HttpGet httpGet = new HttpGet(URLprincipal + query);
			httpGet.addHeader(Peticion.Propiedad1, autorizacion);

			ResponseHandler<String> responseHandler = response -> {
				int status = response.getStatusLine().getStatusCode();
				this.responseCode = status;
				if (status >= 200 && status < 300) {
					HttpEntity responseEntity = response.getEntity();
					return responseEntity != null ? EntityUtils.toString(responseEntity) : null;
				} else {
					throw new ClientProtocolException("Excepcion response status: " + status);
				}
			};
			String responseBody = httpclient.execute(httpGet, responseHandler);
			if (this.responseCode > 300 | this.responseCode == 0) 
				throw new ResponseCodeException(this.responseCode);
			
			System.out.println("Response Code : " + this.responseCode + ". GET request to URL : " + URLprincipal + query);
			return responseBody;
		} catch (MalformedURLException e) {
			System.err.println("Problema de URL mal formada");
			throw new ResponseCodeException(this.responseCode);
		} catch (ConnectException e) {
			System.err.println("Problemas de conexion, revisar server de token");
			throw new ResponseCodeException(this.responseCode);
		} catch (ProtocolException e) {
			System.err.println("Prbblemas: ProtocolException");
			throw new ResponseCodeException(this.responseCode);
		} catch (IOException e) {
			throw new ResponseCodeException(this.responseCode);
		}
	}

	@Override
	public String POST(String query, ArrayList<String> list) throws Exception {
		Token tok = new Token();
		String autorizacion = "Bearer " + tok.getToken();

		if (query == null)
			throw new BadQueryException();

		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

			HttpPost httpPost = new HttpPost(Peticion.URLprincipal + query);
			String inputJ = new String("{\n");
			for (int i = 0; i < list.size(); i++) {
				inputJ = inputJ.concat("  \"" + list.get(i) + "\"");
				i++;
				inputJ = inputJ.concat(": \"" + list.get(i) + "\",\n");
			}
			inputJ = inputJ.concat("}");

			StringEntity stringEntity = new StringEntity(inputJ);
			httpPost.setEntity(stringEntity);

			httpPost.setHeader("Accept", "application/json");
			httpPost.addHeader("Content-Type", "application/json");
			httpPost.addHeader("Authorization", autorizacion);

			ResponseHandler<String> responseHandler = response -> {
				int status = response.getStatusLine().getStatusCode();
				this.responseCode = status;
				if (status >= 200 && status < 300) {
					HttpEntity responseEntity = response.getEntity();
					return responseEntity != null ? EntityUtils.toString(responseEntity) : null;
				} else {
					throw new ClientProtocolException("Excepcion response status: " + status);
				}
			};

			String responseBody = httpclient.execute(httpPost, responseHandler);
			if (this.responseCode > 300 | this.responseCode == 0) 
				throw new ResponseCodeException(this.responseCode);
			
			System.out.println("Response Code : " + responseCode + ". POST request to URL : " + URLprincipal + query);
			return responseBody;
		} catch (MalformedURLException e) {
			System.err.println("Problema de URL mal formada");
		} catch (ConnectException e) {
			System.err.println("Problemas de conexion, revisar server de token");
		} catch (ProtocolException e) {
			System.err.println("Prbblemas: ProtocolException");
		} catch (IOException e) {
			System.err.println("Problemas: IOException");
		}
		return null;
	}

	@Override
	public String PUT(String query) throws Exception {
		String responseBody = new String();
		Token tok = new Token();
		String autorizacion = "Bearer " + tok.getToken();

		if (query == null)
			throw new BadQueryException();

		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

			HttpPut httpPut = new HttpPut("https://accounts.spotify.com/authorize" + query);

			httpPut.addHeader("Content-Type", "application/json");
			httpPut.addHeader("Authorization", autorizacion);
			System.out.println("Ejecutando request: " + httpPut.getRequestLine());

			ResponseHandler<String> responseHandler = response -> {
				int status = response.getStatusLine().getStatusCode();
				this.responseCode = status;
				if (status >= 200 && status < 300) {
					HttpEntity responseEntity = response.getEntity();
					return responseEntity != null ? EntityUtils.toString(responseEntity) : null;
				} else {
					throw new ClientProtocolException("Excepcion response status: " + status);
				}
			};

			responseBody = httpclient.execute(httpPut, responseHandler);
			if (this.responseCode > 300 | this.responseCode == 0) 
				throw new ResponseCodeException(this.responseCode);
			
			System.out.println("Response Code : " + responseCode + ". PUT request to URL : " + URLprincipal + query);
		} catch (MalformedURLException e) {
			System.err.println("Problema de URL mal formada");
		} catch (ConnectException e) {
			System.err.println("Problemas de conexion, revisar server de token");
		} catch (ProtocolException e) {
			System.err.println("Prbblemas: ProtocolException");
		} catch (IOException e) {
			System.err.println("Problemas: IOException");
		}
		return responseBody;
	}

	@Override
	public String getToken() throws Exception {
		String responseBody = new String();

		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			List<NameValuePair> form = new ArrayList<NameValuePair>();
			form.add(new BasicNameValuePair("grant_type", "client_credentials"));
			form.add(new BasicNameValuePair("client_id", this.client_id));
			form.add(new BasicNameValuePair("client_secret", this.client_secret));

			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, Consts.UTF_8);
			HttpPost httpPost = new HttpPost("https://accounts.spotify.com/api/token");
			httpPost.setEntity(entity);

			ResponseHandler<String> responseHandler = response -> {
				int status = response.getStatusLine().getStatusCode();
				this.responseCode = status;
				if (status >= 200 && status < 300) {
					HttpEntity responseEntity = response.getEntity();
					return responseEntity != null ? EntityUtils.toString(responseEntity) : null;
				} else {
					System.err.println("Error al tratar de obtener el token");
					throw new ClientProtocolException("Excepcion response status: " + status);
				}
			};



			responseBody = httpclient.execute(httpPost, responseHandler);
			if (this.responseCode > 300 | this.responseCode == 0) 
				throw new ResponseCodeException(this.responseCode);
			

			System.out.println("Response Code : " + Response.SC_OK + ". " + httpPost.toString() + " TOKEN ACTUALIZADO");
			return responseBody;
		} catch (MalformedURLException e) {
			System.err.println("Problema de URL mal formada");
		} catch (ConnectException e) {
			System.err.println("Problemas de conexion, revisar server de token");
		} catch (ProtocolException e) {
			System.err.println("Prbblemas: ProtocolException");
		} catch (IOException e) {
			System.err.println("Problemas: IOException");
		}
		return null;
	}

	public String login() throws Exception {

		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

			URIBuilder builder = new URIBuilder("GET https://accounts.spotify.com/authorize");
			builder.setParameter("client_id", "4e08fae57975438294428f1a07d3873c").setParameter("response_type", "code")
					.setParameter("redirect_uri", "https://localhost:4200/#/home")
					.setParameter("scope", "user-read-email user-read-private");

			HttpPost httpPost = new HttpPost(builder.build());

			ResponseHandler<String> responseHandler = response -> {
				int status = response.getStatusLine().getStatusCode();
				this.responseCode = status;
				if (status >= 200 && status < 300) {
					HttpEntity responseEntity = response.getEntity();
					return responseEntity != null ? EntityUtils.toString(responseEntity) : null;
				} else {
					throw new ClientProtocolException("Excepcion response status: " + status);
				}
			};

			if (!(this.responseCode >= 200 && this.responseCode < 300))
				throw new ResponseCodeException(this.responseCode);

			String responseBody = httpclient.execute(httpPost, responseHandler);
			if (this.responseCode > 300 | this.responseCode == 0) 
				throw new ResponseCodeException(this.responseCode);
			
			System.out.println(responseBody);
			return responseBody;
		} catch (MalformedURLException e) {
			System.err.println("Problema de URL mal formada");
		} catch (ConnectException e) {
			System.err.println("Problemas de conexion, revisar server de token");
		} catch (ProtocolException e) {
			System.err.println("Prbblemas: ProtocolException");
		} catch (IOException e) {
			System.err.println("Problemas: IOException");
		}
		return null;
	}

}
