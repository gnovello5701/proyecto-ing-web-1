package com.spotiBack.backHost.models.artist;

import com.spotiBack.backHost.models.Item;
import com.spotiBack.backHost.models.auxiliares.Imagen;

public class Artist extends Item{
	
	private String genres[];
	private Imagen images;
	private int popularity;
	
	public Artist() {}
	
	public Artist(String id, String name, String type, String genres[], Imagen images, int popularity ) {
		this.setId(id);
		this.setName(name);
		this.setType(type);
		this.genres = genres;
		this.images = images;
		this.popularity = popularity;
	}
	
	public String[] getAtributos() {
		String atributos[] = new String[6];
		atributos[0] = "id";
		atributos[1] = "name";
		atributos[2] = "type";
		atributos[3] = "images";//VER ACA NO ES ASI
		atributos[4] = "genres";//VER ACA TAMPOCOO ES ASI
		atributos[5] = "popularity";
		return atributos;
	}

	public String[] getGenres() {
		return genres;
	}

	public void setGenres(String[] genres) {
		this.genres = genres;
	}

	public Imagen getImages() {
		return images;
	}

	public void setImages(Imagen images) {
		this.images = images;
	}

	public int getPopularity() {
		return popularity;
	}

	public void setPopularity(int popularity) {
		this.popularity = popularity;
	}
	
	
	
}
