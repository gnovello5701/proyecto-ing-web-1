package com.spotiBack.backHost.models.users;

import com.spotiBack.backHost.models.auxiliares.Imagen;

public class UserProfiles {
	
	private String id;
	private String display_name;
	private String type;
	private Imagen images;
	
	public UserProfiles() {}

	public UserProfiles(String id, String display_name, String type, Imagen images) {
		super();
		this.id = id;
		this.display_name = display_name;
		this.type = type;
		this.images = images;
	}
	
	public String[] getAtributos() {
		String atributos[] = new String[4];
		atributos[0] = "id";
		atributos[1] = "display_name";
		atributos[2] = "type";
		atributos[3] = "imagen";
		return atributos;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisplay_name() {
		return display_name;
	}

	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Imagen getImages() {
		return images;
	}

	public void setImages(Imagen images) {
		this.images = images;
	}

	
}
