package com.spotiBack.backHost.services.exceptions;

public class ResponseCodeException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int responseCode;
	
	public ResponseCodeException(int codigo) {
		super();
		this.responseCode = codigo;
		System.out.println(getMessage());
	}
	
	public ResponseCodeException(String mensaje) {
		super();
		System.out.println(mensaje);
	}
	
	
	public String getMessage() {
		
		String respuesta = "";
		
		switch (this.responseCode) {
		case 200:
			respuesta = "Code: " +this.responseCode + " OK. Todo ha funcionado como se esperaba.";
			break;
		case 201:
			respuesta =  "Code: " +this.responseCode + "El recurso ha creado con éxito en respuesta a la petición POST. La cabecera de situación Location contiene la URL apuntando al nuevo recurso creado.";
			break;
		case 204:
			respuesta =  "Code: " +this.responseCode + "La petición ha sido manejada con éxito y el cuerpo de la respuesta no tiene contenido (como una petición DELETE).";
			break;
		case 304:
			respuesta =  "Code: " +this.responseCode + "El recurso no ha sido modificado. Puede usar la versión en caché.";
			break;
		case 400:
			respuesta =  "Code: " +this.responseCode + "Petición errónea. Esto puede estar causado por varias acciones de el usuario, como proveer un JSON no válido en el cuerpo de la petición, proveyendo parámetros de acción no válidos, etc.";
			break;
		case 401:
			respuesta =  "Code: " +this.responseCode + "Autenticación fallida.";
			break;
		case 403:
			respuesta =  "Code: " +this.responseCode + "El usuario autenticado no tiene permitido acceder a la API final.";
			break;
		case 404:
			respuesta =  "Code: " +this.responseCode + "El recurso pedido no existe.";
			break;
		case 405:
			respuesta =  "Code: " +this.responseCode + "Método no permitido. Por favor comprueba la cabecera Allow por los métodos HTTP permitidos.";
			break;
		case 415:
			respuesta =  "Code: " +this.responseCode + "Tipo de medio no soportado. El tipo de contenido pedido o el número de versión no es válido.";
			break;
		case 422:
			respuesta =  "Code: " +this.responseCode + "La validación de datos ha fallado (en respuesta a una petición POST , por ejemplo). Por favor, comprueba en el cuerpo de la respuesta el mensaje detallado.";
			break;
		case 429:
			respuesta =  "Code: " +this.responseCode + "Demasiadas peticiones. La petición ha sido rechazada debido a un limitación de rango.";
			break;
		case 500:
			respuesta =  "Code: " +this.responseCode + " Error interno del servidor. Esto puede estar causado por errores internos del programa.";
			break;

		default:
			break;
		}
		
		return respuesta;
	}

}
