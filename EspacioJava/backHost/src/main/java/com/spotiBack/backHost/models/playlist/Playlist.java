package com.spotiBack.backHost.models.playlist;

import com.spotiBack.backHost.models.Item;
import com.spotiBack.backHost.models.auxiliares.Imagen;
import com.spotiBack.backHost.models.track.Track;

public class Playlist extends Item {
	
	private Track tracks[];
	private Imagen images;
	
	public Playlist() { }
	
	Playlist(String id, String name, String type, Track tracks[], Imagen images) {
		this.setId(id);
		this.setName(name);
		this.setType(type);
		this.tracks = null;
		if(tracks!=null)
			this.tracks = tracks;
		this.images = images;
	}
	
	public String[] getAtributos() {
		String atributos[] = new String[5];
		atributos[0] = "id";
		atributos[1] = "name";
		atributos[2] = "type";
		atributos[3] = "images";
		atributos[4] = "tracks";
		return atributos;
	}

	public Track[] getTracks() {
		return tracks;
	}

	public void setTracks(Track[] tracks) {
		this.tracks = tracks;
	}

	public Imagen getImages() {
		return images;
	}

	public void setImages(Imagen images) {
		this.images = images;
	}
	
	
}
