package com.spotiBack.backHost.models.album;

import com.spotiBack.backHost.models.Item;
import com.spotiBack.backHost.models.artist.Artist;
import com.spotiBack.backHost.models.auxiliares.Imagen;

public class Album extends Item{
	
	private int total_tracks;
	private Imagen images;
	private Artist artists[];

	public Album() { }
	
	public Album(String id, String name, String type, int total_tracks, Imagen images, Artist artists[]) {
		super();
		this.setId(id);
		this.setName(name);
		this.setType(type);
		this.total_tracks = total_tracks;
		this.images = images;
		this.artists = null;
		if(artists!=null)
			this.artists = artists; 
	}
	
	public String[] getAtributos() {
		String atributos[] = new String[7];
		atributos[0] = "id";
		atributos[1] = "name";
		atributos[2] = "type";
		atributos[3] = "images";
		atributos[4] = "total_tracks";//VER ACA NO ES ASI
		atributos[5] = "artists";//VER ACA TAMPOCOO ES ASI
		return atributos;
	}

	public int getTotal_tracks() {
		return total_tracks;
	}

	public void setTotal_tracks(int total_tracks) {
		this.total_tracks = total_tracks;
	}

	public Imagen getImages() {
		return images;
	}

	public void setImages(Imagen images) {
		this.images = images;
	}

	public Artist[] getArtists() {
		return artists;
	}

	public void setArtists(Artist[] artists) {
		this.artists = artists;
	}

	
	
}
