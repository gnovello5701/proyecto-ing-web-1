package com.spotiBack.backHost.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import com.spotiBack.backHost.services.Peticion;

public class Token {

	private String tok;
	private String dia;
	private String hora;

	public Token() {
		super();
	}

	public String getToken() {
		try {
			tokenMantenimiento();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String a[] = new String[3];
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;
		archivo = new File(System.getProperty("user.dir") + "//src//main//java//com//spotiBack//backHost//util//TOKEN");
		if (archivo.exists()) {
			try {
				archivo = new File(
						System.getProperty("user.dir") + "//src//main//java//com//spotiBack//backHost//util//TOKEN");
				fr = new FileReader(archivo);
				br = new BufferedReader(fr);
				String linea;
				for (int i = 0; (linea = br.readLine()) != null; i++) {
					a[i] = linea;
				}
				return a[0];
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (null != fr) {
						fr.close();
					}
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		} else {
			System.out.println("El archivo no existe");
		}

		return "No habia token";
	}

	public void tokenMantenimiento() throws Exception {

		Calendar calendario = Calendar.getInstance();
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		int dia = calendario.get(Calendar.DAY_OF_MONTH);
		String a[] = new String[3];
		a[1] = new String("dia");
		a[2] = new String("hora");

		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;

		archivo = new File(System.getProperty("user.dir") + "//src//main//java//com//spotiBack//backHost//util//TOKEN");
		if (archivo.exists()) {
			try {
				// Apertura del fichero y creacion de BufferedReader para poder hacer una
				// lectura comoda (disponer del metodo readLine()).
				archivo = new File(
						System.getProperty("user.dir") + "//src//main//java//com//spotiBack//backHost//util//TOKEN");
				fr = new FileReader(archivo);
				br = new BufferedReader(fr);
				String linea;
				for (int i = 0; (linea = br.readLine()) != null; i++) {
					a[i] = linea;
				}
			} catch (ArrayIndexOutOfBoundsException e) {
					System.err.println("TOKEN.txt se encuentra con mas lineas de las que deberia.\nActualizando...");
 			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (null != fr) {
						fr.close();
					}
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		} else {
			System.err.println("El archivo no existe");
		}
		
		if (!(a[1].equals(String.valueOf(dia)) & a[2].equals(String.valueOf(hora)))) {

			try {
				Peticion p = new Peticion();
				String resultado = p.getToken();
				JSONObject jsonObject = new JSONObject(resultado);
				String access_token = (String) jsonObject.get("access_token");
				setTok(access_token);
			} catch (JSONException e) {
				System.err.println("Problema en el armado del JSON para extraer el token luego del overriteToken()");
				return;
			}
			FileWriter fichero = null;
			PrintWriter pw = null;
			try {
				fichero = new FileWriter(
						System.getProperty("user.dir") + "//src//main//java//com//spotiBack//backHost//util//TOKEN");
				pw = new PrintWriter(fichero);
				pw.println(getTok());
				pw.println(dia);
				pw.print(hora);

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (null != fichero)
						fichero.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
	}
	


	public String getTok() {
		return tok;
	}

	public void setTok(String tok) {
		this.tok = tok;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

}
