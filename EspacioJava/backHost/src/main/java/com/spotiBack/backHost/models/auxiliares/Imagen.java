package com.spotiBack.backHost.models.auxiliares;

public class Imagen {
	private String url;
	
	public Imagen() {}
	
	public Imagen(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	

}
