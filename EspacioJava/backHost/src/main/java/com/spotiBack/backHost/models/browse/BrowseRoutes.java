package com.spotiBack.backHost.models.browse;

public class BrowseRoutes {

	public BrowseRoutes() {

	}

	/**
	 * @return String con ruta
	 */

	public static String getGenerosDisponibles() {
		String route = "recommendations/available-genre-seeds";
		return route;
	}

	/**
	 * @return String con ruta
	 */

	public static String getListaBusquedaCategorias() {
		return "browse/categories?country=AR&offset=0&limit=50";
	}

	/**
	 * 
	 * @param id obligatorio
	 * @return String con ruta
	 */

	public static String getBusquedaDeUnaCategoria(String id) {

		if (id == null) {
			return null;
		} else {
			return "browse/categories/" + id + "?country=AR";
		}

	}

	/**
	 * 
	 * @param categoriaId obligatorio
	 * @return String con ruta
	 */

	public static String getBusquedaPlaylistDeUnaCategoria(String categoriaId) {

		if (categoriaId == null) {
			return null;
		} else {
			return "browse/categories/" + categoriaId +"/playlists?country=AR&offset=0&limit=24";
		}

	}

	/**
	 * @return String con ruta
	 */

	public static String getPlaylistsDestacadas() {

		return "browse/featured-playlists?country=AR&limit=8&offset=0";

	}

	/**
	 * @return String con ruta
	 */

	public static String getNewReleases() {

		return "browse/new-releases?country=AR&limit=8&offset=0";

	}

}
