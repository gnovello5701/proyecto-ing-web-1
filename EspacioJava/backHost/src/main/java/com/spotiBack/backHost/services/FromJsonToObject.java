package com.spotiBack.backHost.services;

import org.json.JSONArray;
import org.json.JSONObject;

import com.spotiBack.backHost.models.album.Album;
import com.spotiBack.backHost.models.artist.Artist;
import com.spotiBack.backHost.models.auxiliares.Imagen;
import com.spotiBack.backHost.models.browse.Categoria;
import com.spotiBack.backHost.models.playlist.Playlist;
import com.spotiBack.backHost.models.search.SearchObject;
import com.spotiBack.backHost.models.track.Track;

public class FromJsonToObject implements IJsonToObject {

	public FromJsonToObject() {
	}

	@Override
	public Album setAlbum(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Album();

		Album nuevoAlbum = new Album();
		Imagen images = new Imagen();
		String atributos[] = nuevoAlbum.getAtributos();
		try {
			nuevoAlbum.setId(!jsonObject.isNull(atributos[0]) ? jsonObject.getString(atributos[0]) : null);
			if (nuevoAlbum.getId() == null)
				return new Album();
			nuevoAlbum.setName(!jsonObject.isNull(atributos[1]) ? jsonObject.getString(atributos[1]) : null);
			nuevoAlbum.setType(!jsonObject.isNull(atributos[2]) ? jsonObject.getString(atributos[2]) : null);
			if (!jsonObject.isNull(atributos[3])) {
				JSONArray arregloImagenes = jsonObject.optJSONArray(atributos[3]);
				if (!arregloImagenes.isEmpty()) {
					JSONObject objetoImagen = arregloImagenes.optJSONObject(0);
					String url = objetoImagen.getString("url");
					images.setUrl(url);
					nuevoAlbum.setImages(images);
				} else {
					images.setUrl(null);
					nuevoAlbum.setImages(images);
				}
			} else {
				nuevoAlbum.setImages(null);
			}
			nuevoAlbum.setTotal_tracks(!jsonObject.isNull(atributos[4]) ? jsonObject.getInt(atributos[4]) : 0);
			if (!jsonObject.isNull(atributos[5])) {
				JSONArray arregloArtistas = jsonObject.optJSONArray(atributos[5]);
				if (!arregloArtistas.isEmpty()) {
					Artist artistas[] = new Artist[arregloArtistas.length()];
					for (int j = 0; j < arregloArtistas.length(); j++) {
						JSONObject objectoArtista = arregloArtistas.optJSONObject(j);
						artistas[j] = setArtist(objectoArtista);
					}
					nuevoAlbum.setArtists(artistas);
				} else {
					nuevoAlbum.setArtists(null);
				}
			} else {
				nuevoAlbum.setArtists(null);
			}
			return nuevoAlbum;
		} catch (Exception e) {
			nuevoAlbum = null;
			e.printStackTrace();
		}
		return nuevoAlbum;
	}

	@Override
	public Track[] setAlbumTracks(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Track[0];

		if (!jsonObject.isNull("items")) {
			JSONArray arregloTracks = jsonObject.optJSONArray("items");
			if (!arregloTracks.isEmpty()) {
				Track tracks[] = new Track[arregloTracks.length()];
				for (int i = 0; i < arregloTracks.length(); i++) {
					JSONObject newJsonObject = (JSONObject) arregloTracks.get(i);
					Track nuevoTrack = new Track();
					nuevoTrack = setTrack(newJsonObject);
					tracks[i] = nuevoTrack;
				}
				return tracks;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public Album[] setVariosAlbums(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Album[0];

		if (!jsonObject.isNull("items")) {
			JSONArray arregloAlbums = jsonObject.optJSONArray("items");
			if (!arregloAlbums.isEmpty()) {
				Album albums[] = new Album[arregloAlbums.length()];
				for (int i = 0; i < arregloAlbums.length(); i++) {
					JSONObject newJsonObject = (JSONObject) arregloAlbums.get(i);
					Album nuevoAlbum = new Album();
					nuevoAlbum = setAlbum(newJsonObject);
					albums[i] = nuevoAlbum;
				}
				return albums;
			} else {
				return new Album[0];
			}
		} else {
			return new Album[0];
		}
	}

	@Override
	public Artist setArtist(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Artist();

		Artist nuevoArtist = new Artist();
		Imagen images = new Imagen();
		String atributos[] = nuevoArtist.getAtributos();
		try {
			nuevoArtist.setId(!jsonObject.isNull(atributos[0]) ? jsonObject.getString(atributos[0]) : null);
			if (nuevoArtist.getId() == null)
				return new Artist();
			nuevoArtist.setName(!jsonObject.isNull(atributos[1]) ? jsonObject.getString(atributos[1]) : null);
			nuevoArtist.setType(!jsonObject.isNull(atributos[2]) ? jsonObject.getString(atributos[2]) : null);
			if (!jsonObject.isNull(atributos[3])) {
				JSONArray arregloImagenes = jsonObject.optJSONArray(atributos[3]);
				if (!arregloImagenes.isEmpty()) {
					JSONObject objetoImagen = arregloImagenes.optJSONObject(0);
					String url = objetoImagen.getString("url");
					images.setUrl(url);
					nuevoArtist.setImages(images);
				} else {
					images.setUrl(null);
					nuevoArtist.setImages(images);
				}
			} else {
				nuevoArtist.setImages(null);
			}
			if (!jsonObject.isNull(atributos[4])) {
				JSONArray objectoGeneros = jsonObject.optJSONArray(atributos[4]);
				String generos[] = new String[objectoGeneros.length()];
				for (int j = 0; j < objectoGeneros.length(); j++)
					generos[j] = objectoGeneros.getString(j);
				nuevoArtist.setGenres(generos);
			} else {
				nuevoArtist.setGenres(null);
			}
			nuevoArtist.setPopularity(!jsonObject.isNull(atributos[5]) ? jsonObject.getInt(atributos[5]) : -1);
			return nuevoArtist;
		} catch (Exception e) {
			nuevoArtist = null;
			e.printStackTrace();
		}
		return nuevoArtist;
	}

	@Override
	public Artist[] setVariosArtistas(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Artist[0];

		if (!jsonObject.isNull("artists")) {
			JSONArray arregloArtistas = jsonObject.optJSONArray("artists");
			if (!arregloArtistas.isEmpty()) {
				Artist artist[] = new Artist[arregloArtistas.length()];
				for (int i = 0; i < arregloArtistas.length(); i++) {
					JSONObject newJsonObject = (JSONObject) arregloArtistas.get(i);
					Artist nuevoArtist = new Artist();
					nuevoArtist = setArtist(newJsonObject);
					artist[i] = nuevoArtist;
				}
				return artist;
			} else {
				return new Artist[0];
			}
		} else {
			return new Artist[0];
		}
	}

	@Override
	public Track[] setArtistTracks(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Track[0];

		if (!jsonObject.isNull("tracks")) {
			JSONArray arregloTracks = jsonObject.optJSONArray("tracks");
			if (!arregloTracks.isEmpty()) {
				Track tracks[] = new Track[arregloTracks.length()];
				for (int i = 0; i < arregloTracks.length(); i++) {
					JSONObject newJsonObject = (JSONObject) arregloTracks.get(i);
					Track nuevoTrack = new Track();
					nuevoTrack = setTrack(newJsonObject);
					tracks[i] = nuevoTrack;
				}
				return tracks;
			} else {
				return new Track[0];
			}
		} else {
			return new Track[0];
		}
	}

	@Override
	public Album[] setArtistAlbum(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Album[0];

		if (!jsonObject.isNull("items")) {
			JSONArray arregloAlbums = jsonObject.optJSONArray("items");
			if (!arregloAlbums.isEmpty()) {
				Album albums[] = new Album[arregloAlbums.length()];
				for (int i = 0; i < arregloAlbums.length(); i++) {
					JSONObject newJsonObject = (JSONObject) arregloAlbums.get(i);
					Album nuevoAlbum = new Album();
					nuevoAlbum = setAlbum(newJsonObject);
					albums[i] = nuevoAlbum;
				}
				return albums;
			} else {
				return new Album[0];
			}
		} else {
			return new Album[0];
		}
	}

	@Override
	public Album[] setNewReleases(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Album[0];

		if (!jsonObject.isNull("albums")) {
			JSONObject objectoAlbums = jsonObject.optJSONObject("albums");
			if (!objectoAlbums.isNull("items")) {
				JSONArray arregloAlbums = objectoAlbums.optJSONArray("items");
				if (!arregloAlbums.isEmpty()) {
					Album albums[] = new Album[arregloAlbums.length()];
					for (int i = 0; i < albums.length; i++) {
						JSONObject newJsonObject = (JSONObject) arregloAlbums.get(i);
						Album nuevoAlbum = new Album();
						nuevoAlbum = setAlbum(newJsonObject);
						albums[i] = nuevoAlbum;
					}
					return albums;
				} else {
					return new Album[0];
				}
			} else {
				return new Album[0];
			}
		} else {
			return new Album[0];
		}
	}

	@Override
	public Playlist[] setPlaylistDestacadas(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Playlist[0];

		if (!jsonObject.isNull("playlists")) {
			JSONObject objectoPlaylists = jsonObject.optJSONObject("playlists");
			if (!objectoPlaylists.isNull("items")) {
				JSONArray arregloPlaylists = objectoPlaylists.optJSONArray("items");
				if (!arregloPlaylists.isEmpty()) {
					Playlist playlists[] = new Playlist[arregloPlaylists.length()];
					for (int i = 0; i < playlists.length; i++) {
						JSONObject newJsonObject = (JSONObject) arregloPlaylists.get(i);
						Playlist nuevaPlaylist = new Playlist();
						nuevaPlaylist = setPlaylist(newJsonObject);
						playlists[i] = nuevaPlaylist;
					}
					return playlists;
				} else {
					return new Playlist[0];
				}
			} else {
				return new Playlist[0];
			}
		} else {
			return new Playlist[0];
		}
	}

	@Override
	public Playlist[] setPlaylistCategoria(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Playlist[0];

		return setPlaylistDestacadas(jsonObject);
	}

	@Override
	public Categoria[] setListaCategoria(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Categoria[0];

		if (!jsonObject.isNull("categories")) {
			JSONObject objectoCategorias = jsonObject.optJSONObject("categories");
			if (!objectoCategorias.isNull("items")) {
				JSONArray arregloCategorias = objectoCategorias.optJSONArray("items");
				if (!arregloCategorias.isEmpty()) {
					Categoria categorias[] = new Categoria[arregloCategorias.length()];
					for (int i = 0; i < categorias.length; i++) {
						JSONObject newJsonObject = (JSONObject) arregloCategorias.get(i);
						Categoria nuevaCategoria = new Categoria();
						String atributos[] = nuevaCategoria.getAtributos();
						Imagen icons = new Imagen();
						try {
							nuevaCategoria.setId(
									!newJsonObject.isNull(atributos[0]) ? newJsonObject.getString(atributos[0]) : null);
							if (nuevaCategoria.getId() == null)
								return new Categoria[0];
							nuevaCategoria.setName(
									!newJsonObject.isNull(atributos[1]) ? newJsonObject.getString(atributos[1]) : null);
							if (!newJsonObject.isNull(atributos[3])) {
								JSONArray arregloImagenes = newJsonObject.optJSONArray(atributos[3]);
								if (!arregloImagenes.isEmpty()) {
									JSONObject objetoImagen = arregloImagenes.optJSONObject(0);
									String url = objetoImagen.getString("url");
									icons.setUrl(url);
									nuevaCategoria.setIcon(icons);
								} else {
									icons.setUrl(null);
									nuevaCategoria.setIcon(icons);
								}
							} else {
								nuevaCategoria.setIcon(null);
							}
							categorias[i] = nuevaCategoria;
						} catch (Exception e) {
							nuevaCategoria = null;
							e.printStackTrace();
						}
						if (nuevaCategoria == null) {
							categorias[i] = nuevaCategoria;
						}
					}
					return categorias;
				} else {
					return new Categoria[0];
				}
			} else {
				return new Categoria[0];
			}
		} else {
			return new Categoria[0];
		}
	}

	@Override
	public Playlist[] setPlaylistUser(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Playlist[0];

		if (!jsonObject.isNull("items")) {
			JSONArray arregloPlaylists = jsonObject.optJSONArray("items");
			if (!arregloPlaylists.isEmpty()) {
				Playlist playlists[] = new Playlist[arregloPlaylists.length()];
				for (int i = 0; i < playlists.length; i++) {
					JSONObject newJsonObject = (JSONObject) arregloPlaylists.get(i);
					Playlist nuevaPlaylist = new Playlist();
					nuevaPlaylist = setPlaylist(newJsonObject);
					playlists[i] = nuevaPlaylist;
				}
				return playlists;
			} else {
				return new Playlist[0];
			}
		} else {
			return new Playlist[0];
		}
	}

	@Override
	public Track[] setPlaylistTrack(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Track[0];

		return setAlbumTracks(jsonObject);
	}

	@Override
	public Playlist setPlaylist(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Playlist();

		Playlist nuevaPlaylist = new Playlist();
		String atributos[] = nuevaPlaylist.getAtributos();
		Imagen images = new Imagen();

		try {
			nuevaPlaylist.setId(!jsonObject.isNull(atributos[0]) ? jsonObject.getString(atributos[0]) : null );
			if (nuevaPlaylist.getId()==null)
				return new Playlist();
			nuevaPlaylist.setName(!jsonObject.isNull(atributos[1]) ? jsonObject.getString(atributos[1]) : null );
			nuevaPlaylist.setType(!jsonObject.isNull(atributos[2]) ? jsonObject.getString(atributos[2]) : null );
			if (!jsonObject.isNull(atributos[3])) {
				JSONArray arregloImagenes = jsonObject.optJSONArray(atributos[3]);
				if (!arregloImagenes.isEmpty()) {
					JSONObject objetoImagen = arregloImagenes.optJSONObject(0);
					String url = objetoImagen.getString("url");
					images.setUrl(url);
					nuevaPlaylist.setImages(images);
				} else {
					images.setUrl(null);
					nuevaPlaylist.setImages(images);
				}
			} else {
				nuevaPlaylist.setImages(null);
			}
			if (!jsonObject.isNull(atributos[4])) { 
				JSONObject objectoTracks = jsonObject.optJSONObject(atributos[4]);
				if (!objectoTracks.isNull("items")) {
					JSONArray arregloTracks = objectoTracks.optJSONArray("items");
					Track tracks[] = new Track[arregloTracks.length()];
					if (!arregloTracks.isEmpty()) {
						for (int i = 0; i < arregloTracks.length(); i++) {
							JSONObject objectoDeTrack = (JSONObject) arregloTracks.get(i);
							if(!objectoDeTrack.isNull("track")) {
								JSONObject trackObj =  objectoDeTrack.optJSONObject("track");
								Track nuevoTrack = new Track();
								nuevoTrack = setTrack(trackObj);
								tracks[i] = nuevoTrack;
							} else {
								tracks[i] = null;
							}
						}
						nuevaPlaylist.setTracks(tracks);
					} else {
						nuevaPlaylist.setTracks(null);
					}
				} else {
					nuevaPlaylist.setTracks(null);
				}
			} else {
				nuevaPlaylist.setTracks(null);
			}
			return nuevaPlaylist;
		} catch (Exception e) {
			nuevaPlaylist = null;
			e.printStackTrace();
		}
		return nuevaPlaylist;
	}

	@Override
	public Track setTrack(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Track();

		Track nuevoTrack = new Track();
		String atributos[] = nuevoTrack.getAtributos();
		Imagen images = new Imagen();
		try {

			nuevoTrack.setId(!jsonObject.isNull(atributos[0]) ? jsonObject.getString(atributos[0]) : null);
			if (nuevoTrack.getId() == null)
				return null;
			nuevoTrack.setName(!jsonObject.isNull(atributos[1]) ? jsonObject.getString(atributos[1]) : null);
			nuevoTrack.setType(!jsonObject.isNull(atributos[2]) ? jsonObject.getString(atributos[2]) : null);

			if (!jsonObject.isNull(atributos[3])) {
				JSONArray arregloImagenes = jsonObject.optJSONArray(atributos[3]);
				if (!arregloImagenes.isEmpty()) {
					JSONObject objetoImagen = arregloImagenes.optJSONObject(0);
					String url = objetoImagen.getString("url");
					images.setUrl(url);
					nuevoTrack.setImages(images);
				} else {
					images.setUrl(null);
					nuevoTrack.setImages(images);
				}
			} else {
				nuevoTrack.setImages(null);
			}
			nuevoTrack.setPreview_url(!jsonObject.isNull(atributos[4]) ? jsonObject.getString(atributos[4]) : null);
			if (!jsonObject.isNull(atributos[5])) {
				Album album = setAlbum(jsonObject.getJSONObject(atributos[5]));
				nuevoTrack.setAlbum(album);
			} else {
				nuevoTrack.setAlbum(null);
			}
			if (!jsonObject.isNull(atributos[6])) {
				JSONArray objectoArtistas = jsonObject.optJSONArray(atributos[6]);
				Artist artistas[] = new Artist[objectoArtistas.length()];
				for (int j = 0; j < objectoArtistas.length(); j++)
					artistas[j] = setArtist(objectoArtistas.getJSONObject(j));
				nuevoTrack.setArtistas(artistas);
			} else {
				nuevoTrack.setArtistas(null);
			}
			nuevoTrack.setDuration_ms(!jsonObject.isNull(atributos[7]) ? jsonObject.getInt(atributos[7]) : -1);
			nuevoTrack.setPopularity(!jsonObject.isNull(atributos[8]) ? jsonObject.getInt(atributos[8]) : -1);
			return nuevoTrack;
		} catch (Exception e) {
			nuevoTrack = null;
			e.printStackTrace();
		}
		return nuevoTrack;
	}

	@Override
	public Track[] setVariosTracks(JSONObject jsonObject) {
		if (jsonObject == null)
			return new Track[0];

		return setArtistTracks(jsonObject);
	}

	@Override
	public SearchObject setSearch(JSONObject jsonObject) {
		if (jsonObject == null)
			return new SearchObject();

		SearchObject searchObject = new SearchObject();
		String atributos[] = searchObject.getAtributos();

		if (!jsonObject.isNull(atributos[0])) {
			JSONObject objetoArtists = jsonObject.optJSONObject(atributos[0]);
			if (!objetoArtists.isNull("items")) {
				JSONArray arregloArtists = objetoArtists.optJSONArray("items");
				if (!arregloArtists.isEmpty()) {
					Artist artists[] = new Artist[arregloArtists.length()];
					for (int i = 0; i < arregloArtists.length(); i++) {
						JSONObject jsonArtista = (JSONObject) arregloArtists.get(i);
						Artist nuevoArtista = setArtist(jsonArtista);
						artists[i] = nuevoArtista;
					}
					searchObject.setArtists(artists);
				} else {
					searchObject.setArtists(null);
				}
			} else {
				searchObject.setArtists(null);
			}
		} else {
			searchObject.setArtists(null);
		}

		if (!jsonObject.isNull(atributos[1])) {
			JSONObject objetoTracks = jsonObject.optJSONObject(atributos[1]);
			if (!objetoTracks.isNull("items")) {
				JSONArray arregloTracks = objetoTracks.optJSONArray("items");
				if (!arregloTracks.isEmpty()) {
					Track tracks[] = new Track[arregloTracks.length()];
					for (int i = 0; i < arregloTracks.length(); i++) {
						JSONObject jsonTrack = (JSONObject) arregloTracks.get(i);
						Track nuevoTrack = setTrack(jsonTrack);
						tracks[i] = nuevoTrack;
					}
					searchObject.setTracks(tracks);
				} else {
					searchObject.setTracks(null);
				}
			} else {
				searchObject.setTracks(null);
			}
		} else {
			searchObject.setTracks(null);
		}

		if (!jsonObject.isNull(atributos[2])) {
			JSONObject objetoPlaylists = jsonObject.optJSONObject(atributos[2]);
			if (!objetoPlaylists.isNull("items")) {
				JSONArray arregloPlaylists = objetoPlaylists.optJSONArray("items");
				if (!arregloPlaylists.isEmpty()) {
					Playlist playlists[] = new Playlist[arregloPlaylists.length()];
					for (int i = 0; i < arregloPlaylists.length(); i++) {
						JSONObject jsonPlaylist = (JSONObject) arregloPlaylists.get(i);
						Playlist nuevaPlaylist = setPlaylist(jsonPlaylist);
						playlists[i] = nuevaPlaylist;
					}
					searchObject.setPlaylists(playlists);
				} else {
					searchObject.setPlaylists(null);
				}
			} else {
				searchObject.setPlaylists(null);
			}
		} else {
			searchObject.setPlaylists(null);
		}

		if (!jsonObject.isNull(atributos[3])) {
			JSONObject objetoAlbums = jsonObject.optJSONObject(atributos[3]);
			if (!objetoAlbums.isNull("items")) {
				JSONArray arregloAlbums = objetoAlbums.optJSONArray("items");
				if (!arregloAlbums.isEmpty()) {
					Album albums[] = new Album[arregloAlbums.length()];
					for (int i = 0; i < arregloAlbums.length(); i++) {
						JSONObject jsonAlbum = (JSONObject) arregloAlbums.get(i);
						Album nuevoAlbum = setAlbum(jsonAlbum);
						albums[i] = nuevoAlbum;
					}
					searchObject.setAlbums(albums);
				} else {
					searchObject.setAlbums(null);
				}
			} else {
				searchObject.setAlbums(null);
			}
		} else {
			searchObject.setAlbums(null);
		}

		return searchObject;
	}
}
