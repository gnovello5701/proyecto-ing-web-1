package com.spotiBack.backHost.otherRoutes;

public class PlayerRoutes {

	/**
	 * 
	 * @return String con ruta
	 */

	public static String getTracksEscuchadosRecientemente() {

		return "me/player/recently-played?type=track&limit=8";

	}

	/**
	 * 
	 * @return String con ruta
	 */

	public static String getArtistaReproducidoAhora() {

		return "me/player?market=AR";

	}

	/**
	 * 
	 * @return String con ruta
	 */

	public static String getMisDispositivosDisponibles() {

		return "me/player/devices";

	}

	/**
	 * 
	 * @return String con ruta
	 */

	public static String getTrackReproducidoAhora() {

		return "me/player/currently-playing?market=AR";

	}
	
	public static String skipUserTrack() {
		return "me/player/next";
	}
	public static String previousUserTrack() {
		
		return "me/player/previous";
	}
	public static String pauseUserTrack() {
		
		return "me/player/pause";
	}
	public static String startResumeUserTrack() {
		
		return "me/player/play";
	}
	public static String repeatModeUserTrack(String state) {
		if(state!=null)
			return ("me/player/repeat?state=" + state);
		return "me/player/repeat";
	}
	public static String goToPositionUserTrack(String position_ms) {
		if(position_ms!=null)
			return ("me/player/seek?position_ms=" + position_ms);
		return "me/player/seek?position_ms=0";
	}
	public static String setVolumenUserTrack(String volumen_percent) {
		if(volumen_percent!=null) 
			return ("me/player/volume?volume_percent=" + volumen_percent);
		return "me/player/volume?volume_percent=50";
	}

}
