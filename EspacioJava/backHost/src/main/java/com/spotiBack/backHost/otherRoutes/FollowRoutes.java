package com.spotiBack.backHost.otherRoutes;

public class FollowRoutes {

	/**
	 * 
	 * @param tipo obligatorio
	 * @param idArtista   obligatorio
	 * @return String con ruta
	 */

	public static String getComprobarSiElUsuarioLogueadoSigueArtista(String idArtista) {

		if (idArtista == null) {
			return null;
		} else {
			return "me/following/contains?type=artist&ids=" + idArtista;
		}
	}

	/**
	 * 
	 * @param tipo obligatorio
	 * @return String con ruta
	 */

	public static String getLOQueSigo(String tipo) {

		return "me/following?type=" + tipo + "&limit=8";

	}

	/**
	 * 
	 * @param idUsuarios obligatorio
	 * @param idPlaylist obligatorio
	 * @return String con ruta
	 */

	public static String getComprobarSiUsuarioSigueLaPlaylist(String idUsuarios, String idPlaylist) {

		if ((idUsuarios == null) || (idPlaylist == null)) {
			return null;
		} else {
			return "playlists/" + idPlaylist + "/followers/contains?ids=" + idUsuarios;
		}
	}
}
