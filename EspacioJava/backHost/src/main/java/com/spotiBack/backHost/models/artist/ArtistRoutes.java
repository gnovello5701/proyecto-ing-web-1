package com.spotiBack.backHost.models.artist;

public class ArtistRoutes {

	/**
	 * 
	 * @param artistIds obligatorio
	 * @return String con ruta
	 */
	
	//
	public static String getArtistAlbum(String artistIds) {
		if(artistIds == null) {
			return null;
		}else {
			return "artists/"+artistIds+"/albums?offset=0&limit=8&include_groups=album,single,compilation,appears_on&market=AR";
		}
	}

	/**
	 * 
	 * @param artistIds obligatorio
	 * @return String con ruta
	 */
	public static String getArtistRelacionados(String artistId) {
		String route = "";
		if (artistId != null) {
			route = "artists/" + artistId + "/related-artists";
		} else {
			return null;
		}
		return route;

	}

	/**
	 * 
	 * @param artistIds   obligatorio
	 * @return String con ruta
	 */
	public static String getArtisTopTrak(String artistId) {
		if (artistId == null){
			return null;

		} else {
			return "artists/"+artistId+"/top-tracks?country=AR";
		}
	}

	/**
	 * 
	 * @param artistIds obligatorio
	 * @return String con ruta
	 */
	public static String getArtist(String artistId) {
		String route = "";

		if (artistId != null) {
			route = "artists/" + artistId;
		} else {
			return null;
		}

		return route;

	}

	/**
	 * 
	 * @param VariasArtistId obligatorio
	 * @return String con ruta
	 */
	public static String getVariosArtist(String VariasArtistId) {
		String route = "";
		if (VariasArtistId != null) {
			route = "artists/" + VariasArtistId;
		} else {
			return null;
		}

		return route;
	}

}
