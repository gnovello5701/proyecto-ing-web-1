package com.spotiBack.backHost.models.users;

public class UsersProfileRoutes {

	/**
	 * 
	 * @return String con ruta
	 */

	public static String getPerfilUsuarioLogueado() {

		return "me";

	}

	/**
	 * 
	 * @param idUsuario obligatorio
	 * @return String con ruta
	 */

	public static String getPerfilUsuario(String idUsuario) {

		return "users/" + idUsuario;

	}

}
