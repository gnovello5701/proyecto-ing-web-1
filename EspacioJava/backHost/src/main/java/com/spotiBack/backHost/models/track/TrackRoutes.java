package com.spotiBack.backHost.models.track;

public class TrackRoutes {

	
	/**
	 * 
	 * @param trackId     obligatorio
	 * @return String con ruta
	 */
	
	public static String getAnalisisAudioTrack(String trackId) {

		if (trackId == null) {
			return null;
		} else {
			return "audio-analysis/" + trackId;
		}

	}

	
	/**
	 * 
	 * @param tracksId     obligatorio
	 * @return String con ruta
	 */
	
	public static String getCaracteristicasVariosTracks(String tracksId) {

		if (tracksId == null) {
			return null;
		} else {
			return "audio-features?ids=" + tracksId;
		}

	}
	
	/**
	 * 
	 * @param trackId     obligatorio
	 * @return String con ruta
	 */

	public static String getCaracteristicasTrack(String trackId) {

		if (trackId == null) {
			return null;
		} else {
			return "audio-features/" + trackId;
		}

	}

	/**
	 * 
	 * @param tracksId     obligatorio
	 * @return String con ruta
	 */
	
	public static String getVariosTracks(String tracksId) {

		if (tracksId == null) {
			return null;
		} else {
			return "tracks?ids=" + tracksId + "&market=AR";
		}

	}

	/**
	 * 
	 * @param trackId     obligatorio
	 * @return String con ruta
	 */
	
	public static String getTrack(String trackId) {

		if (trackId == null) {
			return null;
		} else {
			return "tracks/" + trackId + "?market=AR";
		}

	}

}
