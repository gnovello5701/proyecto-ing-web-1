package com.spotiBack.backHost.models.search;

public class SearchRoutes {
	
	
	/**
	 * 
	 * @param tipo     obligatorio
	 * @param nombre     obligatorio
	 * @return String con ruta
	 */
	
	public static String getBuscar(String nombre,String tipo) {

		return "search?q=" + nombre + "&type=" + tipo + "&market=AR&limit=8&offset=0";

	}

}
