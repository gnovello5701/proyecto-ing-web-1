package com.spotiBack.backHost.otherRoutes;

public class LibraryRoutes {

	/**
	 * 
	 * @param idAlbums obligatorio
	 * @return String con ruta
	 */

	public static String getComprobarSiGuardeLosAlbums(String idAlbums) {

		if (idAlbums == null) {
			return null;
		} else {
			return "me/albums/contains?ids=" + idAlbums;
		}
	}

	/**
	 * 
	 * @param tipo obligatorio
	 * @param idTracks   obligatorio
	 * @return String con ruta
	 */

	public static String getComprobarSiGuardeLosTracks(String idTracks) {

		if (idTracks == null) {
			return null;
		} else {
			return "me/tracks/contains?ids=" + idTracks;
		}
	}

	/**
	 * 
	 * @return String con ruta
	 */

	public static String getMisAlbumsGuardados() {

		return "me/albums?limit=8&offset=0";

	}

	/**
	 * 
	 * @return String con ruta
	 */

	public static String getMisTracksGuardados() {

		return "me/tracks?market=AR&limit=8&offset=0";

	}
}
