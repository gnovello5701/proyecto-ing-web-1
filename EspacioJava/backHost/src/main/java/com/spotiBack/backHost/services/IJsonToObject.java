package com.spotiBack.backHost.services;

import org.json.JSONObject;

import com.spotiBack.backHost.models.album.Album;
import com.spotiBack.backHost.models.artist.Artist;
import com.spotiBack.backHost.models.browse.Categoria;
import com.spotiBack.backHost.models.playlist.Playlist;
import com.spotiBack.backHost.models.search.SearchObject;
import com.spotiBack.backHost.models.track.Track;

public interface IJsonToObject {

	// Albums/////////////////////////////////////////////////////////////////////

	public Album setAlbum(JSONObject jsonObject);

	public Track[] setAlbumTracks(JSONObject jsonObject);

	public Album[] setVariosAlbums(JSONObject jsonObject);

	// Artista////////////////////////////////////////////////////////////////////

	public Artist setArtist(JSONObject jsonObject);

	public Artist[] setVariosArtistas(JSONObject jsonObject);

	public Track[] setArtistTracks(JSONObject jsonObject);

	public Album[] setArtistAlbum(JSONObject jsonObject);

	// Browse//////////////////////////////////////////////////////////////////////

	public Album[] setNewReleases(JSONObject jsonObject);

	public Playlist[] setPlaylistDestacadas(JSONObject jsonObject);

	public Playlist[] setPlaylistCategoria(JSONObject jsonObject);

	public Categoria[] setListaCategoria(JSONObject jsonObject);

	// Playlist/////////////////////////////////////////////////////////////////////

	public Playlist[] setPlaylistUser(JSONObject jsonObject);

	public Track[] setPlaylistTrack(JSONObject jsonObject);

	public Playlist setPlaylist(JSONObject jsonObject);

	// Search///////////////////////////////////////////////////////////////////////

	public SearchObject setSearch(JSONObject jsonObject);

	// Tracks///////////////////////////////////////////////////////////////////////

	public Track setTrack(JSONObject jsonObject);

	public Track[] setVariosTracks(JSONObject jsonObject);

}
