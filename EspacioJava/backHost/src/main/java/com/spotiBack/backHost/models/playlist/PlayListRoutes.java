package com.spotiBack.backHost.models.playlist;

public class PlayListRoutes {

	public PlayListRoutes() {

	}

	/**
	 * 
	 * @param playlisId obligatorio
	 * @return String con ruta
	 */

	public static String getPlaylistTracks(String playlisId) {
		if (playlisId == null) {
			return null;
		} else {
			return "playlists/" + playlisId + "/tracks?market=AR&limit=50&offset=0";
		}
	}
	/**
	 * 
	 * @param playlisId obligatorio
	 * @return String con ruta
	 */

	public static String getPlaylist(String playlisId) {

		if (playlisId == null) {
			return null;
		} else {
			return "playlists/" + playlisId + "?market=AR";
		}

	}

	/**
	 * 
	 * @param ususarioId obligatorio
	 * @return String con ruta
	 */

	public static String getListaPlaylistUsuario(String ususarioId) {

		if (ususarioId == null) {
			return null;
		} else {
			return "users/" + ususarioId + "/playlists";
		}

	}

	/**
	 * 
	 * @return String con ruta
	 */

	public static String getListaPlaylistUsuarioLogueado() {

		return "me/playlists";

	}
	
	public static String addTrackToPlaylist(String playlist_id, String uris) {
		if( playlist_id  == null | uris == null ) 
			return null;
		
		return "playlists/" + playlist_id + "/tracks?uris=" + uris;
	}
	
	public static String crearPlaylist(String user_id) {
		if(user_id == null) {
			return null;
		}
		
		return "users/" + user_id + "/playlists";
	}
	
	public static String deleteTrackFromPlaylist(String playlist_id) {
		if(playlist_id==null) 
			return null;
		return "playlists/" + playlist_id + "/tracks";
	}

}
